
<div class="container">
	<div class="row">
		<div class="col-sm-4 offset-sm-4">
			<h3>Добро пожаловать</h3>
			<p>Введите пароль для входа в аккаунт</p>
			<form class="m-t" role="form" action="" method="POST">
				<div class="form-group">
					<input type="login" name="login" class="form-control" placeholder="Логин" required="" maxlength="100">
				</div>
				<div class="form-group">
					<input type="password" name="password" class="form-control" placeholder="Пароль" required="" maxlength="64">
				</div>
				<div class="form-check float-left">
					<input class="form-check-input" name="remember" type="checkbox" id="rememberMe" value="1">
					<label class="form-check-label" for="rememberMe">Запомнить меня</label>
				</div>
				<button type="submit" class="btn btn-primary float-right">Войти в аккаунт</button>
			</form>
		</div>
	</div>
</div>

<script>
	Errors = <?=jsonEncode($errors)?>;
	$.each(Errors, function(name, data) {
		$box = $('[name='+name+']');
		if ($box.length) {
			$box.val(data.value);
			$.each(data.messages, function(i, message) {
				$box.after('<div class="text-danger">'+message+'</div>');
			});
		}
	});
</script>
