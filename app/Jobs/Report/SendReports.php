<?php
/**
 * Reports send job
 */
namespace App\Jobs\Report;
use App\Models\Crm\lead,
	App\Repository\Report\Opt\Leads,
	Classes\Report\SpreadsheetFactory;

class SendReports extends \Core\Jobs\Job
{
	protected 
		$reports,
		$options = [
			'subject' => 'Отчет',
			'body' => 'Файлы во вложении.',
		];
		
	/**
	 * Constructor
	 * @param array $reports
	 * @param array $options
	 */
    public function __construct(Array $reports, Array $options = [])
    {
		$this->reports = $reports;
		foreach ($options as $key=>$val) {
			$this->options[$key] = $val;
		}
    }
	
	/**
	 * Handle job
	 */
    public function handle()
    {
		$result = (object)[
			'status' => false,
			'msg' => ''
		];
		$mailer = email()
			->subject($this->options['subject'])
			->body($this->options['body']);
						
		foreach (config('crm.recipients') as $email) {
			$mailer->to($email);
		}
		foreach ($this->reports as $name=>$path) {
			$mailer->attach($path, $name);
		}
		if ($mailer->send()) {
			$result->status = true;
		} else {
			$result->msg = $mailer->getError();
		}
		return $result;
	}
}
