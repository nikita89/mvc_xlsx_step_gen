<?php
/**
 * Report generate job
 */
namespace App\Jobs\Report;
use App\Repository\Report\Mk\Leads,
	Ufee\Amo\Models\PipelineStatus,
	Classes\Report\SpreadsheetFactory;

class GenerateMK extends GenerateReport
{
	const REPORT_NAME = 'Маркетинг';
	const PIPELINES = [
		1025584, // ОПТ
		1025587 // ОП
	];
	const ROW_HEADER = [
		'A' => 'Месяц.Год', 
		'B' => 'Неделя', 
		'C' => 'Дата обращения', 
		'D' => 'блок тегов №1', 
		'E' => 'блок тегов №2', 
		'F' => 'блок тегов №3', 
		'G' => 'блок тегов №4', 
		'H' => 'Статус', 
		'I' => 'Обращение', 
		'J' => "Целевые\nобращения", 
		'K' => "Встреча\nзапланирована", 
		'L' => 'Встреча', 
		'M' => 'Бронь', 
		'N' => 'Аванс', 
		'O' => 'utm_term', 
		'P' => 'utm_campaign', 
		'Q' => 'utm_content', 
		'R' => 'utm_source', 
		'S' => 'Тип обращения', 
		'T' => 'Проект', 
		'U' => 'Объект', 
		'V' => "Рекламируемый\nпоселок", 
		'W' => 'Рекламный источник', 
		'X' => "Ссылка\nна карточку"
	];
	const BLOCK_TAGS = [
		['Интернет', 'Пресса', 'Лиды Facebook', '#Лид Facebook', 'target.my.com', 'Радио; ТВ', 'Рассылка'],
		['Базы объявлений', 'Обратный звонок', '#Обратный звонок', 'обратный звонок', 'МИРКВАРТИР', 'Ютьюб_видео', 'РБК', 'Коммерсант', 'Новостройки', 'НаНовойРиге', 'Новая Рига', 'ЭХО', 'Мать и Дитя'],
		['CIAN', '2ГИС', 'ДомКлик'],
		['Хайконвершн', 'IP_Ananiev', '#IP_Ananiev', 'Регистратура', '#Регистратура']
	];
	const PREV_STATUS_KEYS = [
		18710071 => 'I',  // Обращение
		18710074 => 'J',  // Целевые обращения
		18710077 => 'K',  // Встреча запланирована
		18710107 => 'L',  // Встреча
		18710110 => 'M',  // Бронь
		18710113 => 'N',  // Аванс
	];
 
	/**
	 * Handle job
	 */
    public function handle()
    {
		$this->spreadsheet = SpreadsheetFactory::createSpreadsheet($this);
		$sheet = $this->spreadsheet->getActiveSheet();
		$sheet->setTitle(self::REPORT_NAME);
		
		foreach (self::ROW_HEADER as $k=>$label) {
			$sheet->setCellValue($k.'1', $label);
			$sheet->getColumnDimension($k)->setAutoSize(true);
		}
		$sheet->getColumnDimension('O')->setAutoSize(false);
		$sheet->getColumnDimension('O')->setWidth(30);
		$sheet->getColumnDimension('P')->setAutoSize(false);
		$sheet->getColumnDimension('P')->setWidth(30);
		$sheet->getColumnDimension('Q')->setAutoSize(false);
		$sheet->getColumnDimension('Q')->setWidth(30);
		$sheet->getColumnDimension('R')->setAutoSize(false);
		$sheet->getColumnDimension('R')->setWidth(30);
		$sheet->getColumnDimension('W')->setAutoSize(false);
		$sheet->getColumnDimension('W')->setWidth(40);
		$sheet->getStyle('A1:X1')->applyFromArray([
			'font' => ['bold' => true],
			'fill' => [
				'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
				'color' => [
					'argb' => 'FFFFDEAD'
				]
			]
		]);
		$sheet->getStyle('A1:X1')->getAlignment()->setWrapText(true);
		$sheet->getStyle('A1:X1')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
		$sheet->getStyle('A1:X1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
		$sheet->freezePane('A1');
		$sheet->freezePane('A2');
		$r = 2;
		$leads = Leads::getPeriod(
			[$this->from->format('Y-m-d H:i:s'), $this->to->format('Y-m-d H:i:s')], self::PIPELINES
		);
		$leads->each(function($lead) use(&$sheet, &$r) {
			$sheet->setCellValue('A'.$r, $lead->createdDate('F Y'));
			$sheet->setCellValue('B'.$r, $lead->createdDate('W-ая неделя m.y'));
			$sheet->setCellValue('C'.$r, $lead->createdDate('d.m.Y'));
			$sheet->setCellValue('D'.$r, join(', ', $lead->sameTagsOf(self::BLOCK_TAGS[0])));
			$sheet->setCellValue('E'.$r, join(', ', $lead->sameTagsOf(self::BLOCK_TAGS[1])));
			$sheet->setCellValue('F'.$r, join(', ', $lead->sameTagsOf(self::BLOCK_TAGS[2])));
			$sheet->setCellValue('G'.$r, join(', ', $lead->sameTagsOf(self::BLOCK_TAGS[3])));
			$sheet->setCellValue('H'.$r, $this->getStatusLabel($lead->status())/* .' ('.$lead->pipeline()->name.' - '.$lead->status()->name.')' */);
			$sheet->setCellValue('I'.$r, (int)$lead->hasStatus(array_search('I', self::PREV_STATUS_KEYS)));
			$sheet->setCellValue('J'.$r, (int)$lead->hasStatus(array_search('J', self::PREV_STATUS_KEYS)));
			$sheet->setCellValue('K'.$r, (int)$lead->hasStatus(array_search('K', self::PREV_STATUS_KEYS)));
			$sheet->setCellValue('L'.$r, (int)$lead->hasStatus(array_search('L', self::PREV_STATUS_KEYS)));
			$sheet->setCellValue('M'.$r, (int)$lead->hasStatus(array_search('M', self::PREV_STATUS_KEYS)));
			$sheet->setCellValue('N'.$r, (int)$lead->hasStatus(array_search('N', self::PREV_STATUS_KEYS)));
			$sheet->setCellValue('O'.$r, $lead->utm_term);
			$sheet->setCellValue('P'.$r, $lead->utm_campaign);
			$sheet->setCellValue('Q'.$r, $lead->utm_content);
			$sheet->setCellValue('R'.$r, $lead->utm_source);
			$sheet->setCellValue('S'.$r, $lead->req_type);
			$sheet->setCellValue('T'.$r, $lead->project);
			$sheet->setCellValue('U'.$r, $lead->object);
			$sheet->setCellValue('V'.$r, $lead->adv_village);
			$sheet->setCellValue('W'.$r, $lead->adv_source);
			$sheet->setCellValue('X'.$r, 'Открыть в CRM');
			$sheet->getCell('X'.$r)->getHyperlink()->setUrl($lead->linkInCrm());

			if ($lead->hasStatus(142)) {
				if ($lead->hasPipeline(1025584)) {
					$sheet->setCellValue('L'.$r, 1);
				}
				if ($lead->hasPipeline(1025587)) {
					$sheet->setCellValue('N'.$r, 1);
				}
			}
			if ($lead->hasStatus(143) && $lead->hasPrevStatusIn(array_keys(self::PREV_STATUS_KEYS))) {
				$key = self::PREV_STATUS_KEYS[$lead->prev_status_id];
				$sheet->setCellValue($key.$r, 1);
			}
			$r++;
		});
		$rl = $r-1;
		$sheet->setCellValue('A'.$r, 'Итого');
		$sheet->setCellValue('I'.$r, '=SUM(I2:I'.$rl.')');
		$sheet->setCellValue('J'.$r, '=SUM(J2:J'.$rl.')');
		$sheet->setCellValue('K'.$r, '=SUM(K2:K'.$rl.')');
		$sheet->setCellValue('L'.$r, '=SUM(L2:L'.$rl.')');
		$sheet->setCellValue('M'.$r, '=SUM(M2:M'.$rl.')');
		$sheet->setCellValue('N'.$r, '=SUM(N2:N'.$rl.')');
		
		$sheet->getStyle('A'.$r)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
		$sheet->getStyle('A'.$r.':X'.$r)->applyFromArray([
			'font' => ['bold' => true]
		]);
		$sheet->getStyle('A'.$r.':X'.$r)->applyFromArray([
			'font' => ['bold' => true],
			'fill' => [
				'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
				'color' => [
					'argb' => 'FFB0C4DE'
				]
			]
		]);
		$sheet->getStyle('A1:X'.$r)->applyFromArray([
			'borders' => [
				'allBorders' => [
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
				]
			]
		]);
		//$this->download();
		
		$writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($this->spreadsheet, 'Xlsx');
		try {
			$writer->save($this->save_path.self::REPORT_NAME.'.xlsx');
			return $this->save_path.self::REPORT_NAME.'.xlsx';
		} catch (\Exception $e) {
			return false;
		}
    }
	
	/**
	 * Get status label
	 * @param PipelineStatus $status
	 */
    protected function getStatusLabel(PipelineStatus $status)
    {
		if ($status->id == 143) {
			return 'Закрыто неудачно';
		}
		if ($status->id == 142 && $status->pipeline->id == 1025587) {
			return 'Закрыто удачно';
		}
		return 'В работе';
	}
}
