<?php
/**
 * Report generate job
 */
namespace App\Jobs\Report;
use App\Models\Crm\Lead,
	App\Repository\Report\Op\Leads,
	Classes\Report\SpreadsheetFactory,
	Jenssegers\Date\Date;

class GenerateOP extends GenerateReport
{
	const REPORT_NAME = 'ОП';
	const PIPELINE_ID = 1025587;
	const STATUS_CHANGES = [
		18710110, // Бронь
		18710113, // Аванс
		142 // Успешно реализовано
	];
	
	const ROW_HEADER = [
		'A' => 'Месяц.Год', 
		'B' => 'Неделя', 
		'C' => 'Дата обращения', 
		'D' => "Тип\nобращения", 
		'E' => 'Объект', 
		'F' => 'Эксперт ОП', 
		'G' => "Встреча\nфакт", 
		'H' => "Бронь\nфакт", 
		'I' => "Аванс\nфакт", 
		'J' => "Договор\nфакт"
	];
 
	/**
	 * Handle job
	 */
    public function handle()
    {
		$responsibles = Lead::responsibles();
		$statusChanges = Leads::getChangesPeriod(
			[$this->from->format('Y-m-d H:i:s'), $this->to->format('Y-m-d H:i:s')], self::PIPELINE_ID, self::STATUS_CHANGES
		);
		$rows = [];
		$range = new \DatePeriod(
			 $this->from, new \DateInterval('P1D'), $this->to
		);
		foreach ($range as $date) {
			$date = new Date(
				$date->getTimestamp()
			);
			$createdLeads = Leads::getCreatedFromDate($date);
			$dayChanges = $statusChanges->get($date->format('Y-m-d'));
			
			foreach ($responsibles as $responsible) {
				foreach (Lead::SHORT_REQ_TYPES as $req_type) {
					foreach (Lead::SHORT_OBJECTS as $object) {

						$row = [
							$date->format('F Y'),
							$date->format('W-ая неделя m.y'),
							$date->format('d.m.Y'),
							$req_type,
							$object,
							$responsible->name,
							$createdLeads->filter(function($lead) use($responsible, $req_type, $object) {
								return $lead['responsible_id'] == $responsible->id && 
										$lead['short_req_type'] == $req_type &&
										$lead['short_object'] == $object;
								})->count(),
							$this->getStatusChangedLeadFromDay($dayChanges, self::STATUS_CHANGES[0], $req_type, $object, $responsible->id)->count(),
							$this->getStatusChangedLeadFromDay($dayChanges, self::STATUS_CHANGES[1], $req_type, $object, $responsible->id)->count(),
							$this->getStatusChangedLeadFromDay($dayChanges, self::STATUS_CHANGES[2], $req_type, $object, $responsible->id)->count()
						];
						$rows[]= $row;
					}
				}
			}
		}
		$this->spreadsheet = SpreadsheetFactory::createSpreadsheet($this);
		$sheet = $this->spreadsheet->getActiveSheet();
		$sheet->setTitle(self::REPORT_NAME);
		
		foreach (self::ROW_HEADER as $k=>$label) {
			$sheet->setCellValue($k.'1', $label);
			$sheet->getColumnDimension($k)->setAutoSize(true);
		}
		$sheet->getStyle('A1:J1')->applyFromArray([
			'font' => ['bold' => true],
			'fill' => [
				'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
				'color' => [
					'argb' => 'FFFFDEAD'
				]
			]
		]);
		$sheet->getStyle('A1:J1')->getAlignment()->setWrapText(true);
		$sheet->getStyle('A1:J1')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
		$sheet->getStyle('A1:J1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
		$sheet->freezePane('A1');
		$sheet->freezePane('A2');
		$r = 2;
		
		foreach ($rows as $row) {
			$sheet->setCellValue('A'.$r, $row[0]);
			$sheet->setCellValue('B'.$r, $row[1]);
			$sheet->setCellValue('C'.$r, $row[2]);
			$sheet->setCellValue('D'.$r, $row[3]);
			$sheet->setCellValue('E'.$r, $row[4]);
			$sheet->setCellValue('F'.$r, $row[5]);
			$sheet->setCellValue('G'.$r, $row[6]);
			$sheet->setCellValue('H'.$r, $row[7]);
			$sheet->setCellValue('I'.$r, $row[8]);
			$sheet->setCellValue('J'.$r, $row[9]);
			$r++;
		}
		$rl = $r-1;
		
		$sheet->setCellValue('A'.$r, 'Итого');
		$sheet->setCellValue('G'.$r, '=SUM(G2:G'.$rl.')');
		$sheet->setCellValue('H'.$r, '=SUM(H2:H'.$rl.')');
		$sheet->setCellValue('I'.$r, '=SUM(I2:I'.$rl.')');
		$sheet->setCellValue('J'.$r, '=SUM(J2:J'.$rl.')');
		
		$sheet->getStyle('A'.$r)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
		$sheet->getStyle('A'.$r.':J'.$r)->applyFromArray([
			'font' => ['bold' => true],
			'fill' => [
				'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
				'color' => [
					'argb' => 'FFB0C4DE'
				]
			]
		]);
		$sheet->getStyle('A1:J'.$r)->applyFromArray([
			'borders' => [
				'allBorders' => [
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
				]
			]
		]);
		//$this->download();
		
		$writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($this->spreadsheet, 'Xlsx');
		$writer->setPreCalculateFormulas(false);
		try {
			$writer->save($this->save_path.self::REPORT_NAME.'.xlsx');
			return $this->save_path.self::REPORT_NAME.'.xlsx';
		} catch (\Exception $e) {
			return false;
		}
    }
	
	/**
	 * Get status changes in day
	 * @param Collection|null $dayChanges
	 * @param integer $status_new
	 * @param string $req_type
	 * @param string $object
	 * @param integer $responsible_id
	 */
    protected function getStatusChangedLeadFromDay(&$dayChanges, $status_new, $req_type, $object, $responsible_id)
    {
		$target = collection();
		if (empty($dayChanges)) {
			return $target;
		}
 		$changes = collection($dayChanges)->find('status_new', $status_new);
		if ($changes->count() == 0) {
			return $target;
		}
		$changes->each(function($change) use(&$target, $req_type, $object, $responsible_id) {
			if (
				$change['responsible_id'] == $responsible_id  && 
				$change['short_req_type'] == $req_type && 
				$change['short_object'] == $object
			) {
				$target->push($change);
			}
		});
		return $target;
	}
}
