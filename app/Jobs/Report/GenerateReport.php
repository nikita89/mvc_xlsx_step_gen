<?php
/**
 * Report job
 */
namespace App\Jobs\Report;
use App\Repository\Report\Mk\Leads,
	Ufee\Amo\Models\PipelineStatus,
	Classes\Report\SpreadsheetFactory;

class GenerateReport extends \Core\Jobs\Job
{
	protected 
		$spreadsheet,
		$save_path;
    public
		$from,
		$to;
		
	/**
	 * Constructor
	 * @param string $from
	 * @param string $to
	 */
    public function __construct($from, $to)
    {
		$this->from = new \DateTime($from);
		$this->to = new \DateTime($to);
		
		$FM = fileManager('/app/Storage/Report/'.$this->from->format('Ymd').'-'.$this->to->format('Ymd').'/');
		if (!$FM->exists()) {
			$FM->createDir();
		}
		$this->save_path = $FM->getPath();
    }
	
	/**
	 * Constructor
	 * @param string $from
	 * @param string $to
	 */
    public function download()
    {
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'.static::REPORT_NAME.'.xlsx"');
		header('Cache-Control: max-age=0');

		$writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($this->spreadsheet, 'Xlsx');
		$writer->setPreCalculateFormulas(false);
		$writer->save('php://output');
		exit;
	}
}
