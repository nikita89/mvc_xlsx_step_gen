<?php
/**
 * Sync amoCRM data controller
 */
namespace App\Controllers\Cron;
use App\Services\Sync\AccountSync,
	App\Services\Sync\CompaniesSync,
	App\Services\Sync\ContactsSync,
	App\Services\Sync\LeadsSync,
	Ufee\Amo\Amoapi;

class Sync extends \Core\Controllers\Controller
{
	/**
	 * CRM to DB sync processing
	 */
    public function process()
    {
		$amo = Amoapi::setInstance(
			config('crm.api')
		);


  		$result_contact = [
  			'contacts' => ContactsSync::syncContacts($amo),
  		];

  		$result_company = [
  			'companies' => CompaniesSync::syncCompanies($amo),
  		];

  		$result = array_merge($result_contact, $result_company);
  		
  		ajaxSuccess($result);
	}
}
