<?php
/**
 * Sync amoCRM data controller
 */
namespace App\Controllers\Cron;

use Components\Curl\Query,
		App\Models\Crm\Contact,
		App\Models\Crm\Company,
		Core\Models\DbModel,
		App\Services\FileGenerator\Contact\ContactCsvGenerator,
		App\Services\FileGenerator\Contact\ContactXlsxGenerator,
		App\Services\FileGenerator\Company\CompanyCsvGenerator,
		App\Services\FileGenerator\Company\CompanyXlsxGenerator;


class GoogleSend extends \Core\Controllers\Controller
{

	/**
	* Directory where files are put
	*/
	private $dir = '/gfdgfdgwrf/';


	/**
	 * Generates csv files from DB
	 */

	private function files_generate(){

		// $contact_gen = new ContactCsvGenerator($this->dir);
		// $contact_gen->generate($contacts);
		// $contacts = Contact::get();

		$contact_gen = new ContactXlsxGenerator($this->dir);
		$contact_gen->generate();

		
		// $company_gen = new CompanyCsvGenerator($this->dir);
		// $company_gen->generate($companies);
		// $companies = Company::get();
		
		$company_gen = new CompanyXlsxGenerator($this->dir);
		$company_gen->generate();
	}


	/**
	 * Send generated files to Google Drive
	 */

	public function send()
	{
		$this->files_generate();

		// $queryparams = config('google_drive.create_folder');
		// $queryparams['folder_name'] = date('d.m.Y');
		// $params['url'] = config('google_drive.base_url');
		// $query = new Query($params['url']);
		// $response = $query->get($queryparams);
		// $params['host_url'] = config('app.hosturl');
		// $params['file_dir'] = $this->dir;
		
		// try {
		// 	if($data = $response->getData()){
		// 		$ar_data = json_decode($data, true);
		// 		if(isset($ar_data['folder_id'])){
		// 			$queryparams = config('google_drive.upload_file');
		// 			$queryparams['folder_id'] = $ar_data['folder_id'];
		// 			$this->send_file((new ContactXlsxGenerator($this->dir))->get_gilename(), $params, $queryparams);
		// 			$this->send_file((new CompanyXlsxGenerator($this->dir))->get_filename(), $params, $queryparams);
		// 		}
		// 		else
		// 			throw new \Exception("Ошибка создания директории {$queryparams['folder_name']} {$data}");

		// 		view('mainsend.sendfile/success', []);
		// 	}
		// } catch (\Exception $e) {
		// 		view('mainsend.sendfile/error', [
		// 			'error' => $e->getMessage()
		// 		]);
		// }
	}

	/**
	*	Send file
	*/

	protected function send_file(string $filename, array $params, array $queryparams){
		$queryparams['file_name'] = $filename;
		$queryparams['file_url'] = $params['host_url'].$params['file_dir'].$filename;
		$query = new Query($params['url']);
		$response = $query->get($queryparams);
		if($data = $response->getData()){
			$ar_data = json_decode($data, true);
			if(isset($ar_data['error'])){
				throw new \Exception("Ошибка загрузки файла {$filename}. Ошибка {$ar_data['error']}");
			}
		}
	}
}
