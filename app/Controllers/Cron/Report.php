<?php
/**
 * Report generate controller
 */
namespace App\Controllers\Cron;
use App\Jobs;

class Report extends \Core\Controllers\Controller
{	
	/**
	 * Weekly report, in 09:00 hours
	 */
    public function weekly()
    {
		$reports = [];
		$from = date('Y-m-d 00:00:00', strtotime('monday -14 weeks'));
		$to = date('Y-m-d 23:59:59', strtotime('sunday previous week'));
		
		$Mk = new Jobs\Report\GenerateMK($from, $to);
		$reports['Маркетинг.xlsx'] = process($Mk);
		
		$Op = new Jobs\Report\GenerateOP($from, $to);
		$reports['ОП.xlsx'] = process($Op);
		
		$Opt = new Jobs\Report\GenerateOPT($from, $to);
		$reports['ОПТ.xlsx'] = process($Opt);

		$SendReports = new Jobs\Report\SendReports($reports, [
			'subject' => 'Недельный отчет'
		]);
		$sended = process($SendReports);
		ajaxResponse($sended);
	}

	/**
	 * Quarterly report, in 09:30 hours
	 */
    public function quarterly()
    {
		$reports = [];
		$from = date('Y-m-1 00:00:00', strtotime('-3 months'));
		$to = date('Y-m-t 23:59:59', strtotime('previous month'));

		$Mk = new Jobs\Report\GenerateMK($from, $to);
		$reports['Маркетинг-квартал.xlsx'] = process($Mk);
		
		$Op = new Jobs\Report\GenerateOP($from, $to);
		$reports['ОП-квартал.xlsx'] = process($Op);
		
		$Opt = new Jobs\Report\GenerateOPT($from, $to);
		$reports['ОПТ-квартал.xlsx'] = process($Opt);
		
		$SendReports = new Jobs\Report\SendReports($reports, [
			'subject' => 'Квартальный отчет'
		]);
		$sended = process($SendReports);
		ajaxResponse($sended);
	}
}
