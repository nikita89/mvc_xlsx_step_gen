<?php
/**
 * CRM web-hook controller
 */
namespace App\Controllers\Crm;
use App\Models\Crm\Lead;

class Hook extends \Core\Controllers\Controller
{
	private 
		$entity,
		$mode,
		$hook,
		$data;
	
    public function __construct()
    {
        parent::__construct();

		$data = $this->request->post();
		$this->entity = key($data);
		if (is_null($this->entity)) {
			error(400, 'Invalid web-hook request');
		}
		$this->mode = key($data[$this->entity]);
		$this->hook = $this->entity.'_'.$this->mode;
		$this->data = $data[$this->entity][$this->mode];
	}
	
	/**
	 * Lead deleted
	 * @param array $item
	 */
    private function leads_delete($item)
    {
		$logger = logger('webhook/'.$this->hook.'.log');
		$logger->log('Lead deleted id: '.$item['id']);
		
		if ($lead = Lead::find($item['id'])) {
			$lead->remove();
			$logger->log('db record removed');
		}
	}
	
	/**
	 * Hook handler
	 */
    public function handler()
    {
		$method = $this->hook;
		if (method_exists($this, $method)) {
			foreach ($this->data as $item) {
				$this->$method($item);
			}
		}
		ajaxSuccess();
	}
}
