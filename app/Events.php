<?php
/**
 * Web Application Events boot
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace App;
use Core\Models\Event;

class Events extends \Core\Containers\BootableSingle
{
	protected function _boot(\Components\Events $events)
	{
		$events->registerListeners('App\Events\Event', [
			'\App\Events\Listeners\EventListener'
		]);
		$events->registerSubscribers([
			'\App\Events\Subscribers\SomeSubscriber',
		]);
	}
}
