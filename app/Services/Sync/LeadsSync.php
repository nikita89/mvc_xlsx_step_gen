<?php
/**
 * CRM Leads sync service
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace App\Services\Sync;
use App\Models\Crm\Lead,
	App\Models\Crm\Statuschange,
	Ufee\Amo\Amoapi,
	Components\Database\Connection as DB;

abstract class LeadsSync
{
	/**
	 * Sync leads
	 * @param Amoapi $amo
	 * @param integer $max_rows - limit for part
	 * @return integer
	 */
	public static function syncLeads(Amoapi $amo, $max_rows = 3000)
	{
		DB::link()->rawQuery('SET FOREIGN_KEY_CHECKS = 0');
		$FM = fileManager('/app/Storage/Sync/');
		
		$last_time = $FM->get('leads', 1);
		$leads = $amo->leads()
					 ->modifiedFrom($last_time-1)
					 ->maxRows($max_rows)
					 ->listing();
		$leads->sortBy('updated_at');
		$leads->each(function($lead) use(&$last_time) {
			$data = [
				'id' => $lead->id,
				'responsible_id' => $lead->responsible_user_id,
				'pipeline_id' => $lead->pipeline_id,
				'status_id' => $lead->status_id,
				'date_create' => date('Y-m-d H:i:s', $lead->created_at),
				'last_modified' => date('Y-m-d H:i:s', $lead->updated_at),
				'exp_op' => $lead->cf('Эксперт')->getValue(),
				'req_type' => $lead->cf('Тип обращения')->getValue(),
				'short_req_type' => self::shortReqType($lead->cf('Тип обращения')->getValue()),
				'project' => $lead->cf('Проект')->getValue(),
				'object' => $lead->cf('Объект')->getValue(),
				'short_object' => self::shortObject($lead->cf('Проект')->getValue(), $lead->cf('Объект')->getValue()),	
				'utm_term' => $lead->cf('utm_term')->getValue(),
				'utm_campaign' => $lead->cf('utm_campaign')->getValue(),
				'utm_content' => $lead->cf('utm_content')->getValue(),
				'utm_source' => $lead->cf('utm_source')->getValue(),
				'adv_village' => $lead->cf('Рекламируемый поселок')->getValue(),
				'adv_source' => $lead->cf('Рекламный источник')->getValue(),
				'tags' => join('||', $lead->tags),
				'leave_reason' => $lead->cf('Причина отказа')->getValue()
			];
			Lead::insertOrUpdate($data);
			$last_time = $lead->updated_at;
		});
		$FM->put('leads', $last_time);
		return $leads->count();
	}
	
	/**
	 * Sync status changes from leads
	 * @param Amoapi $amo
	 * @param integer $max_rows - limit for part
	 * @return integer
	 */
	public static function syncLeadStatusChanges(Amoapi $amo, $max_rows = 3000)
	{
		DB::link()->rawQuery('SET FOREIGN_KEY_CHECKS = 0');
		$FM = fileManager('/app/Storage/Sync/');

		$last_time = $FM->get('notes', 1);
		$notes = $amo->notes()
					 ->modifiedFrom($last_time-1)
					 ->maxRows($max_rows)
					 ->type(3);
		$notes->sortBy('updated_at');
		$notes->each(function($note) use(&$last_time, $amo) {
			
			if ($lead = Lead::find($note->element_id)) {
				$data = [
					'id' => $note->id,
					'lead_id' => $note->element_id,
					'pipeline_old' => $note->params->PIPELINE_ID_OLD,
					'status_old' => $note->params->STATUS_OLD,
					'pipeline_new' => $note->params->PIPELINE_ID_NEW,
					'status_new' => $note->params->STATUS_NEW,
					'date_create' => date('Y-m-d H:i:s', $note->created_at)
				];
				$statusChange = Statuschange::insertOrUpdate($data);
				$lead->prev_pipeline_id = $note->params->PIPELINE_ID_OLD;
				$lead->prev_status_id = $note->params->STATUS_OLD;
				$lead->save();
				
				if ($pipeline = $amo->account->pipelines->byId($statusChange->pipeline_new)) {
					if ($status = $pipeline->statuses->byId($statusChange->status_new)) {
						
						foreach($status->prevs as $prevStatus) {
							if (!Statuschange::get(['lead_id' => $note->element_id, 'status_new' => $prevStatus->id])->first()) {
								Statuschange::insertOrUpdate([
									'id' => $note->id.''.$prevStatus->id,
									'lead_id' => $note->element_id,
									'pipeline_old' => $note->params->PIPELINE_ID_OLD,
									'status_old' => $note->params->STATUS_OLD,
									'pipeline_new' => $pipeline->id,
									'status_new' => $prevStatus->id,
									'date_create' => $statusChange->date_create
								]);
							}
							$status = $prevStatus;
						}
					}
				}
			}
			$last_time = $note->updated_at;
		});
		$FM->put('notes', $last_time);
		return $notes->count();
	}
	
    /**
     * Get short request type version
	 * @param string $req_type
	 * @return string
     */
	protected static function shortReqType($req_type)
	{
		if (in_array($req_type, ['Звонок', 'Обратный звонок', 'Звонок с КПП'])) {
			return 'Звонок';
		}
		return $req_type;
	}
	
    /**
     * Get short object version
	 * @param string $project
	 * @param string $object
	 * @return string
     */
	protected static function shortObject($project, $object)
	{
		if ($project == 'Futuro Park' && $object == 'Таунхаус') {
			return 'ФП';
		}
		if ($project == 'Park Fonte' && $object == 'Таунхаус') {
			return 'ПФ';
		}
		if ($project == 'Park Avenue' && $object == 'Таунхаус') {
			return 'ПА';
		}
		if ($project == 'Park Avenue' && $object == 'Квартира') {
			return 'АПА';
		}
		if ($project == 'Иное') {
			return 'Иное';
		}
		return null;
	}
}
