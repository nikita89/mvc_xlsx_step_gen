<?php
/**
 * CRM Leads sync service
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace App\Services\Sync;
use App\Models\Crm\Company,
	App\Models\Crm\Statuschange,
	Ufee\Amo\Amoapi,
	Components\Database\Connection as DB;

abstract class CompaniesSync
{

	/**
	 * Sync leads
	 * @param Amoapi $amo
	 * @param integer $max_rows - limit for part
	 * @return integer
	 */
	public static function syncCompanies(Amoapi $amo, $max_rows = 1000)
	{
		DB::link()->rawQuery('SET FOREIGN_KEY_CHECKS = 0');

		$FM = fileManager('/app/Storage/Sync/');
		$last_time = $FM->get('companies', 1);

		$account = $amo->account;
		$users = $account->users;
		$companies = $amo->companies()
					 ->modifiedFrom($last_time-1)
					 ->maxRows($max_rows)
					 ->listing();
		$companies->sortBy('updated_at');
		$companies_names = [];
		$companies->each(function($company) use(&$last_time, $users, &$companies_names) {
			$data = [
				'id' => $company->id,
				'name' => $company->name,
				'created_by' => $company->createdUser ? $company->createdUser->name : '',
				'tags' => implode(',', $company->tags),
				'sys_cf_responsible' => $company->responsibleUser ? $company->responsibleUser->name : '',
				'sys_cf_phones' => implode(',', $company->cf('Телефон')->getValues()),
				'sys_cf_emails' => implode(',', $company->cf('Email')->getValues()),
				'cf_field' => $company->cf('Отрасль')->getValue() ,
				'cf_reduction' => $company->cf('Скидка')->getValue(),
				'cf_ID' => $company->cf('ID')->getValue(),
				'cf_PCID' => $company->cf('PCID')->getValue(),
				'cf_contact' => $company->cf('Контактное лицо')->getValue(),
				'cf_type' => implode(',', $company->cf('Тип')->getValues()),
				'created' => date('Y-m-d H:i:s', $company->created_at)
			];

			Company::insertOrUpdate($data);
			$last_time = $company->updated_at;
		});
		$FM->put('companies', $last_time);
		return $companies->count();
	}

}
