<?php
/**
 * CRM Leads sync service
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace App\Services\Sync;
use App\Models\Crm\Contact,
	App\Models\Crm\Statuschange,
	Ufee\Amo\Amoapi,
	Components\Database\Connection as DB;

abstract class ContactsSync
{

	/**
	 * Sync leads
	 * @param Amoapi $amo
	 * @param integer $max_rows - limit for part
	 * @return integer
	 */
	public static function syncContacts(Amoapi $amo, $max_rows = 1000)
	{
		DB::link()->rawQuery('SET FOREIGN_KEY_CHECKS = 0');
		$FM = fileManager('/app/Storage/Sync/');

		$last_time = $FM->get('contacts', 1);

		$account = $amo->account;
		$users = $account->users;
		$contacts = $amo->contacts()
					 ->modifiedFrom($last_time-1)
					 ->maxRows($max_rows)
					 ->listing();
		$contacts->sortBy('updated_at');
		$contacts->each(function($contact) use(&$last_time, $users) {
			$data = [
				'id' => $contact->id,
				'name' => $contact->name,
				'company_name' => $contact->company ? $contact->company->name : '',
				'created_by' => $contact->createdUser ? $contact->createdUser->name : '',
				'tags' => implode(',', $contact->tags),
				'sys_cf_responsible' => $contact->responsibleUser ? $contact->responsibleUser->name : '',
				'sys_cf_phones' => implode(',', $contact->cf('Телефон')->getValues()),
				'sys_cf_emails' => implode(',', $contact->cf('Email')->getValues()),
				'sys_cf_position' => $contact->cf('Должность')->getValue() ,
				'cf_notinc_dispatch' => $contact->cf('Не включать в рассылку')->getValue(),
				'cf_city_phone' => $contact->cf('Телефон городской 1')->getValue(),
				'cf_city_phone_2' => $contact->cf('Телефон городской 2')->getValue(),
				'cf_city_phone_3' => $contact->cf('Телефон городской 3')->getValue(),
				'cf_mob_phone' => $contact->cf('Телефон мобильный 1')->getValue(),
				'cf_mob_phone_2' => $contact->cf('Телефон мобильный 2')->getValue(),
				'cf_dont_call' => $contact->cf('Не звонить')->getValue(),
				'cf_black_list' => $contact->cf('Черный список')->getValue(),
				'cf_birth_date' => $contact->cf('Дата рождения')->getValue(),
				'cf_club_cart' => $contact->cf('Клубная карта')->getValue(),
				'cf_ID' => $contact->cf('ID')->getValue(),
				'cf_AccountID' => $contact->cf('AccountID1')->getValue(),
				'cf_AccountID' => $contact->cf('AccountID1')->getValue(),
				'cf_KO_times' => $contact->cf('Сколько раз проживал в КО')->getValue(),
				'cf_live_cost' => $contact->cf('Сумма услуг проживания')->getValue(),
				'cf_gender' => $contact->cf('Пол')->getValue(),
				'cf_child_birth_date_1' => $contact->cf('ДР ребенка 1')->getValue(),
				'cf_child_name_1' => $contact->cf('Имя ребенка 1')->getValue(),
				'cf_child_birth_date_2' => $contact->cf('ДР ребенка 2')->getValue(),
				'cf_child_name_2' => $contact->cf('Имя ребенка 2')->getValue(),
				'cf_child_birth_date_3' => $contact->cf('ДР ребенка 3')->getValue(),
				'cf_child_name_3' => $contact->cf('Имя ребенка 3')->getValue(),
				'cf_marriage_date' => $contact->cf('Дата свадьбы')->getValue(),
				'cf_spouse_birth_date' => $contact->cf('ДР супруга')->getValue(),
				'cf_spouse_name' => $contact->cf('Имя супруга')->getValue(),
				'cf_note' => $contact->cf('Примечание')->getValue(),
				'cf_user_agreement' => $contact->cf('CF_NAME_USER_AGREEMENT')->getValue() ? 'Да' : 'Нет',
				'created' => date('Y-m-d H:i:s', $contact->created_at)
			];

			Contact::insertOrUpdate($data);
			$last_time = $contact->updated_at;
		});
		$FM->put('contacts', $last_time);
		return $contacts->count();
	}

}
