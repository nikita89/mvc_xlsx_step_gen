<?php
/**
 * Abstract File Generator class
 */
namespace App\Services\FileGenerator\company;

use Core\Models\Collections\Model,
		Components\Curl\Query,
		App\Models\Crm\Company,
		App\Services\FileGenerator\XlsxGenerator;

class companyXlsxGenerator extends XlsxGenerator{

	/**
	* @var string xlsx title
	*/

	protected $title = 'Компании';

	/**
	* @var string $modelclass Class of model
	*/

	protected $modelclass = Company::class;

	/**
	* @var string xlsx desctiption
	*/

	protected $description = 'Компании';


	/**
	* @var string table range
	*/

	protected  $range = 'A1:L1';


	/**
	* @var string filename
	*/

	protected $filename = 'Companies.xlsx';


	/**
	 *@var array human readable map of fields
	 */

	protected $field_map = [
		'A' => 'Название компании',
		'B' => 'Создатель',
		'C' => 'Теги',
		'D' => 'Ответственный',
		'E' => 'Телефоны',
		'F' => 'Email',
		'G' => 'Отрасль' ,
		'H' => 'Скидка',
		'I' => 'ID',
		'J' => 'PCID',
		'K' => 'Контактное лицо',
		'L' => 'Тип',
		//'M' => 'Дата создания'
	];


	/**
	 * Write models
	 * @param Core\Models\Collections\Model $collection.
	 * @param integer $r number of row to start from
	 * @param PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $sheet
	 * @return PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $sheet
	 */

	public function write_models(Model $collection, int &$r, \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $sheet){
		$collection->each(function($company) use(&$sheet, &$r) {
			$sheet->setCellValue('A'.$r, $company->name);
			$sheet->setCellValue('B'.$r, $company->created_by);
			$sheet->setCellValue('C'.$r, $company->tags);
			$sheet->setCellValue('D'.$r, $company->sys_cf_responsible);
			$sheet->setCellValue('E'.$r, $company->sys_cf_phones);
			$sheet->setCellValue('F'.$r, $company->sys_cf_emails);
			$sheet->setCellValue('G'.$r, $company->cf_field);
			$sheet->setCellValue('H'.$r, $company->cf_reduction);
			$sheet->setCellValue('I'.$r, $company->cf_ID);
			$sheet->setCellValue('J'.$r, $company->cf_PCID);
			$sheet->setCellValue('K'.$r, $company->cf_contact);
			$sheet->setCellValue('L'.$r, $company->cf_type);
			// $sheet->setCellValue('M'.$r, $company->created);
			$r++;
		});
	}
	

	/**
	 * Set custom column width
	 * @param PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $sheet
	 */

	public function custom_column_width(\PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $sheet){
		$sheet->getColumnDimension('A')->setAutoSize(false);
		$sheet->getColumnDimension('A')->setWidth(30);
	}
}

