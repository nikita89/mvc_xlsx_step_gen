<?php
/**
 * Abstract File Generator class
 */
namespace App\Services\FileGenerator\Company;

use Core\Models\Collections\Model,
		Components\Curl\Query,
		App\Models\Crm\Contact,
		App\Services\Sync\ContactsSync,
		App\Services\FileGenerator\CsvGenerator;

class CompanyCsvGenerator extends CsvGenerator{

	/**
	* @var string filename
	*/

	protected $filename = 'company.csv';


	/**
	 * @var array human readable map of fields
	 */

	protected $field_map = [
		'name' => 'Название компании',
		'created_by' => 'Создатель',
		'tags' => 'Теги',
		'sys_cf_responsible' => 'Ответственный',
		'sys_cf_phones' => 'Телефоны',
		'sys_cf_emails' => 'Email',
		'cf_field' => 'Отрасль' ,
		'cf_reduction' => 'Скидка',
		'cf_ID' => 'ID',
		'cf_PCID' => 'PCID',
		'cf_contact' => 'Контактное лицо',
		'cf_type' => 'Тип',
		'created' => 'Дата создания'
	];

}

