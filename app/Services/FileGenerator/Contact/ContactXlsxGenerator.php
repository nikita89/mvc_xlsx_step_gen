<?php
/**
 * Abstract File Generator class
 */
namespace App\Services\FileGenerator\Contact;

use Core\Models\Collections\Model,
		Components\Curl\Query,
		App\Models\Crm\Contact,
		App\Services\FileGenerator\XlsxGenerator;

class ContactXlsxGenerator extends XlsxGenerator{

	/**
	* @var string xlsx title
	*/

	protected $title = 'Контакты';

	/**
	* @var string $modelclass Class of model
	*/

	protected $modelclass = Contact::class;


	/**
	* @var string xlsx desctiption
	*/

	protected $description = 'Контакты';


	/**
	* @var string table range
	*/

	protected  $range = 'A1:AI1';


	/**
	* @var string filename
	*/

	protected $filename = 'Contacts.xlsx';


	/**
	 *@var array human readable map of fields
	 */

	protected $field_map = [
		'A' => 'Наименование',
		'B' => 'Название компании',
		'C' => 'Создатель',
		'D' => 'Теги',
		'E' => 'Ответственный',
		'F' => 'Телефоны',
		'G' => 'Email',
		'H' => 'Должность' ,
		'I' => 'Не включать в рассылку',
		'J' => 'Телефон городской 1',
		'K' => 'Телефон городской 2',
		'L' => 'Телефон городской 3',
		'M' => 'Телефон мобильный 1',
		'N' =>'Телефон мобильный 2',
		'O' => 'Не звонить',
		'P' => 'Черный список',
		'Q' => 'Дата рождения',
		'R' => 'Клубная карта',
		'S' => 'ID',
		'T' => 'AccountID1',
		'U' => 'Сколько раз проживал в КО',
		'V' => 'Сумма услуг проживания',
		'W' => 'Пол',
		'X' => 'ДР ребенка 1',
		'Y' => 'Имя ребенка 1',
		'Z' => 'ДР ребенка 2',
		'AA' => 'Имя ребенка 2',
		'AB' => 'ДР ребенка 3',
		'AC' => 'Имя ребенка 3',
		'AD' => 'Дата свадьбы',
		'AE' => 'ДР супруга',
		'AF' => 'Имя супруга',
		'AG' => 'Примечание',
		'AH' => 'Пользовательское соглашение',
		//'AI' => 'Дата создания'
	];


	/**
	 * Write models
	 * @param Core\Models\Collections\Model $collection.
	 * @param integer $r number of row to start from
	 * @param PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $sheet
	 * @return PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $sheet
	 */

	public function write_models(Model $collection, int &$r, \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $sheet){
		$collection->each(function($contact) use(&$sheet, &$r) {
			$sheet->setCellValue('A'.$r, $contact->name);
			$sheet->setCellValue('B'.$r, $contact->company_name);
			$sheet->setCellValue('C'.$r, $contact->created_by);
			$sheet->setCellValue('D'.$r, $contact->tags);
			$sheet->setCellValue('E'.$r, $contact->sys_cf_responsible);
			$sheet->setCellValue('F'.$r, $contact->sys_cf_phones);
			$sheet->setCellValue('G'.$r, $contact->sys_cf_emails);
			$sheet->setCellValue('H'.$r, $contact->sys_cf_position);
			$sheet->setCellValue('I'.$r, $contact->cf_notinc_dispatch);
			$sheet->setCellValue('J'.$r, $contact->cf_city_phone);
			$sheet->setCellValue('K'.$r, $contact->cf_city_phone_2);
			$sheet->setCellValue('L'.$r, $contact->cf_city_phone_3);
			$sheet->setCellValue('M'.$r, $contact->cf_mob_phone);
			$sheet->setCellValue('N'.$r, $contact->cf_mob_phone_2);
			$sheet->setCellValue('O'.$r, $contact->cf_dont_call);
			$sheet->setCellValue('P'.$r, $contact->cf_black_list);
			$sheet->setCellValue('Q'.$r, $contact->cf_birth_date);
			$sheet->setCellValue('R'.$r, $contact->cf_club_cart);
			$sheet->setCellValue('S'.$r, $contact->cf_ID);
			$sheet->setCellValue('T'.$r, $contact->cf_AccountID);
			$sheet->setCellValue('U'.$r, $contact->cf_KO_times);
			$sheet->setCellValue('V'.$r, $contact->cf_live_cost);
			$sheet->setCellValue('W'.$r, $contact->cf_gender);
			$sheet->setCellValue('X'.$r, $contact->cf_child_birth_date_1);
			$sheet->setCellValue('Y'.$r, $contact->cf_child_name_1);
			$sheet->setCellValue('Z'.$r, $contact->cf_child_birth_date_2);
			$sheet->setCellValue('AA'.$r, $contact->cf_child_name_2);
			$sheet->setCellValue('AB'.$r, $contact->cf_child_birth_date_3);
			$sheet->setCellValue('AC'.$r, $contact->cf_child_name_3);
			$sheet->setCellValue('AD'.$r, $contact->cf_marriage_date);
			$sheet->setCellValue('AE'.$r, $contact->cf_spouse_birth_date);
			$sheet->setCellValue('AF'.$r, $contact->cf_spouse_name);
			$sheet->setCellValue('AG'.$r, $contact->cf_note);
			$sheet->setCellValue('AH'.$r, $contact->cf_user_agreement);
			//$sheet->setCellValue('AI'.$r, $contact->created);
			$r++;
		});
	}
	

	/**
	 * Set custom column width
	 * @param PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $sheet
	 */

	public function custom_column_width(\PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $sheet){
		$sheet->getColumnDimension('A')->setAutoSize(false);
		$sheet->getColumnDimension('A')->setWidth(30);
		$sheet->getColumnDimension('B')->setAutoSize(false);
		$sheet->getColumnDimension('B')->setWidth(30);
		$sheet->getColumnDimension('F')->setAutoSize(false);
		$sheet->getColumnDimension('F')->setWidth(30);
		$sheet->getColumnDimension('G')->setAutoSize(false);
		$sheet->getColumnDimension('G')->setWidth(30);
		$sheet->getColumnDimension('D')->setAutoSize(false);
		$sheet->getColumnDimension('D')->setWidth(30);
		$sheet->getColumnDimension('P')->setAutoSize(false);
		$sheet->getColumnDimension('P')->setWidth(30);
		$sheet->getColumnDimension('AG')->setAutoSize(false);
		$sheet->getColumnDimension('AG')->setWidth(30);
	}


}

