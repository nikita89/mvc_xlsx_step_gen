<?php
/**
 * Abstract File Generator class
 */
namespace App\Services\FileGenerator\Contact;

use Core\Models\Collections\Model,
		Components\Curl\Query,
		App\Models\Crm\Contact,
		App\Services\Sync\ContactsSync,
		App\Services\FileGenerator\CsvGenerator;

class ContactCsvGenerator extends CsvGenerator{

	/**
	* @var string filename
	*/

	protected $filename = 'contacts.csv';


	/**
	 *@var array human readable map of fields
	 */

	protected $field_map = [
		'name' => 'Наименование',
		'company_name' => 'Название компании',
		'created_by' => 'Создатель',
		'tags' => 'Теги',
		'sys_cf_responsible' => 'Ответственный',
		'sys_cf_phones' => 'Телефоны',
		'sys_cf_emails' => 'Email',
		'sys_cf_position' => 'Должность' ,
		'cf_notinc_dispatch' => 'Не включать в рассылку',
		'cf_city_phone' => 'Телефон городской 1',
		'cf_city_phone_2' => 'Телефон городской 2',
		'cf_city_phone_3' => 'Телефон городской 3',
		'cf_mob_phone' => 'Телефон мобильный 1',
		'cf_mob_phone_2' =>'Телефон мобильный 2',
		'cf_dont_call' => 'Не звонить',
		'cf_black_list' => 'Черный список',
		'cf_birth_date' => 'Дата рождения',
		'cf_club_cart' => 'Клубная карта',
		'cf_ID' => 'ID',
		'cf_AccountID' => 'AccountID1',
		'cf_KO_times' => 'Сколько раз проживал в КО',
		'cf_live_cost' => 'Сумма услуг проживания',
		'cf_gender' => 'Пол',
		'cf_child_birth_date_1' => 'ДР ребенка 1',
		'cf_child_name_1' => 'Имя ребенка 1',
		'cf_child_birth_date_2' => 'ДР ребенка 2',
		'cf_child_name_2' => 'Имя ребенка 2',
		'cf_child_birth_date_3' => 'ДР ребенка 3',
		'cf_child_name_3' => 'Имя ребенка 3',
		'cf_marriage_date' => 'Дата свадьбы',
		'cf_spouse_birth_date' => 'ДР супруга',
		'cf_spouse_name' => 'Имя супруга',
		'cf_note' => 'Примечание',
		'cf_user_agreement' => 'Пользовательское соглашение',
		'created' => 'Дата создания'
	];

}

