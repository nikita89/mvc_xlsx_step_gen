<?php
/**
 * Abstract File Generator class
 */
namespace App\Services\FileGenerator;

use Core\Models\Collections\Model,
		Components\Curl\Query;

class CsvGenerator extends Generator{

	/**
	* @var string filedir
	*/

	protected $dir = '/generated_file_dir/';

	/**
	* @var string filename
	*/

	protected $filename = '';


	/**
	 *@var array human readable map of fields
	 */

	protected  $field_map = [];


	/**
	* @return string filename
	*/

	public function get_filename(){
		return $this->filename;
	} 


	/**
	 *@return array map
	 */

	protected function getMap(){
		return $this->field_map;
	}


	/**
	 * Generate csv file
	 * @param Core\Models\Collections\Model $collection.
	 */

	public function generate(Model $collection){
		$csv = fopen($this->save_path.$this->filename, 'w');
		// getting list of models
		$items = $collection->toArray();
		$first_row = $items[0];
		unset($first_row['id']);
		unset($first_row['created_at']);
		unset($first_row['modified_at']);
		$fields_keys = array_keys($first_row);
		$fields_names = array_map(function($item){
			if(array_key_exists($item, $this->field_map))
				return $this->field_map[$item];
		}, $fields_keys);

		fputcsv($csv, $fields_names, ';');

		foreach ($items as $item) {
			unset($item['id']);
			unset($item['created_at']);
			unset($item['modified_at']);
			fputcsv($csv, array_values($item), ';');
		}
		fclose($csv);
	}

}

