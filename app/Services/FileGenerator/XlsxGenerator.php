<?php
/**
 * Abstract File Generator class
 */
namespace App\Services\FileGenerator;

use Core\Models\Collections\Model,
	Components\Database\Connection as Db,
	Components\Curl\Query,
	PhpOffice\PhpSpreadsheet\Spreadsheet,
	PhpOffice\PhpSpreadsheet\IOFactory;


class XlsxGenerator extends Generator{

	/**
	* @var string xlsx title
	*/

	protected $title = '';

	/**
	* @var string xlsx desctiption
	*/

	protected $description = '';


	/**
	* @var string table range
	*/

	protected  $range = '';


	/**
	* @var string filedir
	*/

	protected  $dir = '/generated_file_dir/';

	/**
	* @var string filename
	*/

	protected $filename = '';

	/**
	 * int $totalCount Total count of raw
	 */

	protected $totalCount = 0;


	/**
	* Name of file keeping generating info
	*/

	protected $generation_info_filename = 'file_genaration_info';


	/**
	* Limit for file lines generated per request
	*/

	protected $line_limit = 250;


	/**
	 *@var array human readable map of fields
	 */

	protected $field_map = [];


	/**
	* @return string filename
	*/

	public function get_filename(){
		return $this->filename;
	}


	/**
	 *@return array map
	 */

	protected function getMap(){
		return $this->field_map;
	}

	/**
	 * Style header
	 * @param PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $sheet 
	 * @param string $range
	 */

	protected function style_header($sheet, string $range = ''){
		if(!empty($range)){
			$sheet->getStyle($range)->applyFromArray([
				'font' => ['bold' => true],
				'fill' => [
					'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
					'color' => [
						'argb' => 'FFFFDEAD'
					]
				]
			]);
			$sheet->getStyle($range)->getAlignment()->setWrapText(true);
			$sheet->getStyle($range)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
			$sheet->getStyle($range)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
		}
	}


	/**
	 * Generate file
	 */

	public function generate(){

		$this->reset_generating();

		// init step writing

		$r = 2;
		$spreadsheet = $this->createSpreadsheet();
		$cur_date = date('y-m-d');
		if(!file_exists($this->generation_info_filename)){
			$save_data = array(
				'last_day' => $cur_date
			);
			file_put_contents($this->generation_info_filename, serialize($save_data));
		}
		else{
			$save_data = unserialize(file_get_contents($this->generation_info_filename));
			if(array_key_exists($this->get_filename(), $save_data)){
				if($save_data[$this->get_filename()] !== 'end'){
					$spreadsheet = IOFactory::load($this->save_path.$this->filename);
					$r = $save_data[$this->get_filename()] + 1;
				}
				else
					return false;
			}
		}

		// query for test total count
		$collection = $this->get_limit_model($this->modelclass, 1);

		$limit = $this->line_limit;
		if(file_exists($this->generation_info_filename)){
			$save_data = unserialize(file_get_contents($this->generation_info_filename));
			if(array_key_exists($this->get_filename(), $save_data))
				if($save_data[$this->get_filename()] !== 'end'){
					// $ex = $this->totalCount . ' ' . ($save_data[$this->get_filename()] - 1);
					// exit($ex);
					if((int) $this->totalCount === (int)($save_data[$this->get_filename()] - 1)){
						$save_data[$this->get_filename()] = 'end';
						file_put_contents($this->generation_info_filename, serialize($save_data));
						return false;
					}
					else
						$limit = [$this->line_limit, $save_data[$this->get_filename()] - 1];
				}
				else
					return false;
		}



		$collection = $this->get_limit_model($this->modelclass, $limit);


		$sheet = $spreadsheet->getActiveSheet();
		foreach ($this->field_map as $k=>$label) {
			$sheet->setCellValue($k.'1', $label);
			$sheet->getColumnDimension($k)->setAutoSize(true);
		}

		$this->style_header($sheet, $this->range);
		$this->custom_column_width($sheet);
		$this->write_models($collection, $r, $sheet);

		$writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
		try {
			//save offset for step writing
			$save_data = unserialize(file_get_contents($this->generation_info_filename));
			// if($collection->count() < $this->line_limit or $collection->count() == 0){
			// 	$save_data[$this->get_filename()] = 'end';
			// }
			// else
				$save_data[$this->get_filename()] = $r - 1;

			file_put_contents($this->generation_info_filename, serialize($save_data));
			$writer->save($this->save_path.$this->filename);
			// exit(json_encode($this->totalCount));
			// exit(json_encode($limit));
			return $this->save_path.$this->filename;
		} catch (\Exception $e) {
			return false;
		}

	}


	/**
	 * Reset generating file
	 */

	public function reset_generating(){
		if(file_exists($this->generation_info_filename)){
			$save_data = unserialize(file_get_contents($this->generation_info_filename));
			foreach ($save_data as $infokey => $infovalue) {
				if($infokey !== 'last_day' and $infovalue !== 'end')
					return false;
			}

			$cur_date = date('y-m-d');
			if($save_data['last_day'] !== $cur_date){
				unlink($this->generation_info_filename);
			}
		}
	}


	/**
	 * @param string $modelclass Model class
	 * @param int | array $limit Array to define SQL limit in format Array ($count, $offset)
	 */

	protected function get_limit_model(string $modelclass, $limit){

		new \ReflectionClass($modelclass);


		$dblink = Db::link()->fromTable($modelclass::getTableName());
		$selected = $dblink->withTotalCount()->get($limit);
		$this->totalCount = $dblink->totalCount;
        if ($selected) {
			foreach ($selected as $data) {
				$object = new $modelclass($data);
				$models[]= $object;
			}
        }
        return new Model($models, $modelclass);
	}


	/**
	* Create Spreadsheet
	* @return Spreadsheet
	*/

    public function createSpreadsheet()
    {
		$spreadsheet = new Spreadsheet();
		$spreadsheet->getProperties()
			->setCompany('Команда F5')
			->setCreator('Vladislav Ionov')
			->setTitle($this->title)
			->setDescription($this->description);

		return $spreadsheet;
	}

	/**
	 * Write models
	 * @param Core\Models\Collections\Model $collection.
	 * @param integer $r number of row to start from
	 * @param PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $sheet
	 */

	public function write_models(Model $collection, int &$r, \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $sheet){

	}

	/**
	 * Set custom column width
	 * @param PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $sheet
	 */

	public function custom_column_width(\PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $sheet){

	}

}

