<?php
/**
 * Abstract File Generator class
 */
namespace App\Services\FileGenerator;

use Core\Models\Collections\Model;

abstract class Generator{

	/**
	* @var string filedir
	*/

	protected $dir = '';


	/**
	 * Generate file
	 */

	abstract public function generate();

	/**
	 * Constructor
	 */
    public function __construct(string $dir = '')
    {
		if(empty($dir))
			$dir = $this->dir;
		$FM = fileManager($dir);
		if (!$FM->exists()) {
			$FM->createDir();
		}
		$this->save_path = $FM->getPath();
		return $this;
    }

}

