<?php
/**
 * Web Application Route boot
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace App;
use 		App\Models\Crm\Company;
class Routes extends \Core\Containers\RoutesContainer
{
	protected function web(\Components\Route $route)
	{
		$route->get('/', function() {
			view('welcome', ['title' => Company::get()->count()]);
		}, 'Main');
		$route->get('sendfiles', 'App\Controllers\Cron\GoogleSend@send');
		$route->get('cron/sync/process', 'App\Controllers\Cron\Sync@process');
		$route->post('crm/hook', 'App\Controllers\Crm\Hook@handler');
	}

	protected function cli(\Components\Route $route)
	{
		$route->cli('cron/report/weekly', 'App\Controllers\Cron\Report@weekly');
		$route->cli('cron/report/quarterly', 'App\Controllers\Cron\Report@quarterly');
	}
}
