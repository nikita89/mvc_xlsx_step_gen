<?php
/**
 * Leads Repository
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace App\Repository\Report\Mk;
use App\Models\Crm\Lead,
	Core\Models\Collections\Model as ModelCollection,
	Components\Database\Connection as Db;

abstract class Leads
{
    /**
     * Get lead data
	 * @param array $dates
	 * @param array $pipelines
	 * @return ModelCollection
     */
	public static function getPeriod($dates, $pipelines)
	{
		$leads = new ModelCollection();
		$selected = Db::link()->where('date_create', $dates, 'BETWEEN')
							  ->where('pipeline_id', $pipelines, 'IN')
							  ->fromTable('model_lead')
							  ->get(null, 'id, pipeline_id, status_id, date_create, utm_term, utm_campaign, utm_content, utm_source, req_type, project, object, adv_village, adv_source, tags');
        if ($selected) {
			foreach ($selected as $raw) {
				$leads->push(
					new Lead($raw)
				);
			}
        }
        return $leads;
	}
}
