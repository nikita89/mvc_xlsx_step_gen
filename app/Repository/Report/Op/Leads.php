<?php
/**
 * Leads Repository
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace App\Repository\Report\Op;
use App\Models\Crm\Lead,
	App\Models\Crm\Statuschange,
	Core\Models\Collections\Model as ModelCollection,
	Components\Database\Connection as Db;

abstract class Leads
{
    /**
     * Get created leads
	 * @param Date $date
	 * @return Collection
     */
	public static function getCreatedFromDate($date)
	{
		$leads = collection();
		Db::link()->where('date_create', [$date->format('Y-m-d 00:00:00'), $date->format('Y-m-d 23:59:59')], 'BETWEEN');
		$selected = Db::link()->fromTable('model_lead')
							  ->get(null, 'id, responsible_id, short_req_type, short_object');
        if ($selected) {
			foreach($selected as $raw) {
				$leads->push($raw);
			}
        }
        return $leads;
	}
	
    /**
     * Get lead status changes
	 * @param array $dates
	 * @param integer $pipeline_id
	 * @param array $statuses
	 * @return Collection
     */
	public static function getChangesPeriod($dates, $pipeline_id, $statuses)
	{
		$changes = [];
		Db::link()->join('model_lead leads', 'changes.lead_id=leads.id', 'LEFT');
		$selected = Db::link()->where('changes.date_create', $dates, 'BETWEEN')
							  ->where('changes.pipeline_new', $pipeline_id)
							  ->where('changes.status_new', $statuses, 'IN')
							  ->orderBy('changes.date_create', 'ASC')
							  ->fromTable('model_statuschange changes')
							  ->get(null, 'changes.lead_id, changes.pipeline_new, changes.status_new, DATE_FORMAT(changes.date_create, \'%Y-%m-%d\') as date_create, leads.responsible_id, leads.short_req_type, leads.short_object');
		if ($selected) {
			foreach ($selected as $raw) {
				$changes[$raw['lead_id'].$raw['status_new']]= $raw;
			}
        }
        return collection($changes)->groupBy('date_create');
	}
}
