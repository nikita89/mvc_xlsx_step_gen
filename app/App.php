<?php
/**
 * Web Application stages
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace App;
use Core\Application;

class App extends \Core\Containers\AppContainer
{
	protected function _boot(Application $app)
	{
		\Jenssegers\Date\Date::setLocale('ru');
		\PhpOffice\PhpSpreadsheet\Settings::setLocale('ru');
	}
	protected function _shutdown(Application $app)
	{
		
	}
}
