<?php
/**
 * AmoCRM Lead Model
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace App\Models\Crm;
use Ufee\Amo\Amoapi,
	Jenssegers\Date\Date;

class Contact extends \Core\Models\DbModel
{

	protected static
		$_ai = false;
	protected
		$writable = [
			'name',
			'company_name',
			'created_by',
			'tags',
			'sys_cf_responsible',
			'sys_cf_position',
			'sys_cf_emails',
			'sys_cf_phones',
			'cf_notinc_dispatch',
			'cf_city_phone',
			'cf_city_phone_2',
			'cf_city_phone_3',
			'cf_mob_phone',
			'cf_mob_phone_2',
			'cf_dont_call',
			'cf_black_list',
			'cf_birth_date',
			'cf_club_cart',
			'cf_ID',
			'cf_AccountID',
			'cf_KO_times',
			'cf_live_cost',
			'cf_gender',
			'cf_child_birth_date_1',
			'cf_child_name_1',
			'cf_child_birth_date_2',
			'cf_child_name_2',
			'cf_child_birth_date_3',
			'cf_child_name_3',
			'cf_marriage_date',
			'cf_spouse_birth_date',
			'cf_spouse_name',
			'cf_note',
			'cf_user_agreement'
		];


	protected static
		$_crm;

	/**
	 * Get crm api
 * @return Amoapi
	 */
	public static function crm()
	{
		if (is_null(self::$_crm)) {
			self::$_crm = Amoapi::setInstance(
				config('crm.api')
			);
		}
		return self::$_crm;
	}
}
