<?php
/**
 * AmoCRM Lead Model
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace App\Models\Crm;
use Ufee\Amo\Amoapi,
	Jenssegers\Date\Date;

class Lead extends \Core\Models\DbModel
{
	use \Core\Models\Traits\Has;

	const SHORT_REQ_TYPES = [
		'Звонок', 'Заявка', 'Архив'
	];
	const SHORT_OBJECTS = [
		'ФП', 'ПФ', 'ПА', 'АПА', 'Иное'
	];
	protected static
		$_ai = false;
	protected
		$writable = [
			'last_modified',
			'responsible_id',
			'pipeline_id',
			'status_id',
			'prev_pipeline_id',
			'prev_status_id',
			'date_create',
			'exp_op',
			'req_type',
			'short_req_type',
			'project',
			'object',
			'short_object',
			'utm_term',
			'utm_campaign',
			'utm_content',
			'utm_source',
			'adv_village',
			'adv_source',
			'tags',
			'leave_reason'
		],
		$_created,
		$_pipeline,
		$_status;
	protected static
		$_crm;

    /**
     * Status access
	 * @param string|null $format - Y-m-d
	 * @return Date|string
     */
	public function createdDate($format = null)
	{
		if (is_null($this->_created)) {
			$this->_created = new Date($this->date_create);
		}
		if (is_string($format)) {
			return $this->_created->format($format);
		}
		return $this->_created;
	}

    /**
     * Pipeline access
	 * @return Pipeline
     */
	public function pipeline()
	{
		if (is_null($this->_pipeline)) {
			$this->_pipeline = self::crm()->account->pipelines->byId($this->pipeline_id);
		}
		return $this->_pipeline;
	}

    /**
     * Has lead pipeline
	 * @param integer $status_id
	 * @return bool
     */
	public function hasPipeline($pipeline_id)
	{
		return $this->pipeline_id == $pipeline_id;
	}

    /**
     * Status access
	 * @return PipelineStatus
     */
	public function status()
	{
		if (is_null($this->_status) && $this->pipeline()) {
			$this->_status = $this->pipeline()->statuses->byId($this->status_id);
		}
		return $this->_status;
	}

    /**
     * Has lead status
	 * @param integer $status_id
	 * @return bool
     */
	public function hasStatus($status_id)
	{
		return $this->status_id == $status_id;
	}

    /**
     * Has lead prev status in array
	 * @param array $status_ids
	 * @return bool
     */
	public function hasPrevStatusIn(Array $status_ids)
	{
		return in_array($this->prev_status_id, $status_ids);
	}

    /**
     * Has tag found
	 * @param string $name
	 * @return bool
     */
	public function hasTag($name)
	{
		return in_array($name, $this->tags);
	}

    /**
     * Get intersect tags
	 * @param array $tags
	 * @return array
     */
	public function sameTagsOf(Array $tags)
	{
		return array_intersect($this->tags, $tags);
	}

    /**
     * Responsibles all
	 * @return UserCollection
     */
	public static function responsibles()
	{
		return self::crm()->account->users;
	}

    /**
     * Get Lead Responsible
	 * @return Responsible
     */
	public function responsible()
	{
		return self::crm()->account->users->find('id', $this->responsible_id)->first();
	}

    /**
     * Experts OP
	 * @return
     */
	public static function opExperts()
	{
		return self::crm()->account->customFields->leads->find('name', 'Эксперт')->first()->enums;
	}

    /**
     * Get Lead status changes
	 * @return Collection
     */
	public function changes()
	{
		return $this->hasLinked(Statuschange::class);
	}

    /**
     * Protect tags access
	 * @return array
     */
	protected function tags_access()
	{
		return explode('||', $this->attributes['tags']);
	}

    /**
     * Get CRM link for lead
	 * @return string
     */
	public function linkInCrm()
	{
		return 'https://'.self::crm()->getAuth('domain').'.amocrm.'.self::crm()->getAuth('zone').'/leads/detail/'.$this->id;
	}

    /**
     * Get crm api
	 * @return Amoapi
     */
	public static function crm()
	{
		if (is_null(self::$_crm)) {
			self::$_crm = Amoapi::setInstance(
				config('crm.api')
			);
		}
		return self::$_crm;
	}
}
