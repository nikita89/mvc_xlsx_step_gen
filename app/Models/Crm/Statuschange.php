<?php
/**
 * AmoCRM Lead status change Model
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace App\Models\Crm;
use Jenssegers\Date\Date;

class Statuschange extends \Core\Models\DbModel
{
	use \Core\Models\Traits\Belong;
	
	protected static 
		$_ai = false;
	protected 
		$writable = [
			'lead_id', 
			'pipeline_old',
			'status_old', 
			'pipeline_new',
			'status_new',  
			'date_create'
		],
		$_created;
	
    /**
     * Status access
	 * @param string|null $format - Y-m-d
	 * @return Date|string
     */
	public function createdDate($format = null)
	{
		if (is_null($this->_created)) {
			$this->_created = new Date($this->date_create);
		}
		if (is_string($format)) {
			return $this->_created->format($format);
		}
		return $this->_created;
	}
	
    /**
     * Get Statuschange Responsible
	 * @return Responsible
     */
	public function responsible()
	{
		return $this->belongsTo(Responsible::class);
	}
	
    /**
     * Get Statuschange Lead
	 * @return Lead
     */
	public function lead()
	{
		return $this->belongsTo(Lead::class);
	}
}
