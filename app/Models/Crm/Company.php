<?php
/**
 * AmoCRM Lead Model
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace App\Models\Crm;
use Ufee\Amo\Amoapi,
	Jenssegers\Date\Date;

class Company extends \Core\Models\DbModel
{

	protected static
		$_ai = false;
	protected
		$writable = [
			'name',
			'created_by',
			'tags',
			'sys_cf_responsible',
			'sys_cf_emails',
			'sys_cf_phones',
			'cf_field',
			'cf_reduction',
			'cf_ID',
			'cf_PCID',
			'cf_contact',
			'cf_type',
			'created'
		];

		protected static
			$_crm;

	/**
	 * Get crm api
 * @return Amoapi
	 */
	public static function crm()
	{
		if (is_null(self::$_crm)) {
			self::$_crm = Amoapi::setInstance(
				config('crm.api')
			);
		}
		return self::$_crm;
	}
}
