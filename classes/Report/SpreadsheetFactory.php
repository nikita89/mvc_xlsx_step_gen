<?php
/**
 * Create Spreadsheets
 */
namespace Classes\Report;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

abstract class SpreadsheetFactory
{
    /**
     * Create Spreadsheet
	 * @param Job $Job
	 * @return Spreadsheet
     */
    public static function createSpreadsheet(\Core\Jobs\Job $Job)
    {
		$spreadsheet = new Spreadsheet();
		$spreadsheet->getProperties()
			->setCompany('Команда F5')
			->setCreator('Vladislav Ionov')
			->setTitle('Отчет - '.$Job::REPORT_NAME)
			->setDescription('Период: '.$Job->from->format('d.m.Y').' - '.$Job->to->format('d.m.Y'));
		return $spreadsheet;
	}
}
