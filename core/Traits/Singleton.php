<?php
/**
 * Web Application Trait
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace Core\Traits;

trait Singleton
{
	protected static
		$_instance;

    /**
     * Get instance
	 * @return object
     */
    public static function getInstance()
    {
		if (!is_null(static::$_instance)) {
			return static::$_instance;
		}
		return static::setInstance(new static);
	}
	
    /**
     * Set instance
	 * @param object $instance
	 * @return object
     */
    private static function setInstance($instance)
    {
		static::$_instance = $instance;
		return static::$_instance;
	}
}
