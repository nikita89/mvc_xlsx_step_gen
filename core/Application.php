<?php
/**
 * Web Application core class
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace Core;
use Core\Traits,
	Components\Response,
	Components\Security;

class Application
{
	use Traits\Singleton;
	
	private static
		$_start_time,
		$_start_micro_time,
		$_registered_instances = [
			'app' => 'Core\Application',
		],
		$_registered_models = [];

    /**
     * Application run
	 * @return void
     */
    public function run()
    {
		if (!is_null(static::$_start_time)) {
			throw new \Exception('Aplication is already runned');
		}
		static::$_start_time = $this->getTime();
		static::$_start_micro_time = microtime(true);
		
		Security\Crypto::init();
		if (config('output_buffer')) {
			Response::obStart();
		}
		$this->registerInstance('Components\Events', 'events');
		$this->registerInstance('Components\Request', 'request');
		$this->registerInstance('Components\Session', 'session');
		$this->registerInstance('Components\Auth', 'auth');

		register_shutdown_function(
			[\App\App::getInstance(), '__shutdown']
		);
		$this->registerInstance('Components\Route', 'route');
		$this->getRegisteredInstanceAlias('route')->navigate();
	}
	
    /**
     * Set registered Instance
	 * @param string $namespace
	 * @param string $key
	 * @return void
     */
    public function registerInstance($namespace, $key)
    {
		if (!array_key_exists($key, static::$_registered_instances)) {
			static::$_registered_instances[$key] = $namespace;
			if (method_exists($namespace, '_register')) {
				forward_static_call_array(
					[$namespace, '_register'], getStaticMethodRegisteredArgs($namespace, '_register')
				);
			}
		}
	}
	
    /**
     * Get registered instance
	 * @param string $namespace
	 * @return mixed
     */
    public function getRegisteredInstance($namespace)
    {
		if (!in_array($namespace, static::$_registered_instances)) {
			return null;
		}
		return $namespace::getInstance();
	}
	
    /**
     * Get registered instance alias
	 * @param string $key
	 * @return mixed
     */
    public function getRegisteredInstanceAlias($key)
    {
		if (!array_key_exists($key, static::$_registered_instances)) {
			return null;
		}
		$namespace = static::$_registered_instances[$key];
		return $namespace::getInstance();
	}
	
    /**
     * Set registered Model
	 * @param string $namespace
	 * @param string $key
	 * @return void
     */
    public function registerModel($namespace, $key)
    {
		if (!array_key_exists($key, static::$_registered_models)) {
			static::$_registered_models[$key] = $namespace;
			if (method_exists($namespace, '_register')) {
				forward_static_call_array(
					[$namespace, '_register'], getStaticMethodRegisteredArgs($namespace, '_register')
				);
			}
		}
	}
	
    /**
     * Get registered
	 * @param string $namespace
	 * @param mixed $arg
	 * @return mixed
     */
    public function getRegistered($namespace, $arg)
    {
		if ($model = $this->getRegisteredModel($namespace, $arg)) {
			return $model;
		}
		return $this->getRegisteredInstance($namespace);
	}
	
    /**
     * Get registered Model
	 * @param string $namespace
	 * @param mixed $arg
	 * @return mixed
     */
    public function getRegisteredModel($namespace, $arg)
    {
		if (!in_array($namespace, static::$_registered_models)) {
			return null;
		}
		return $namespace::find($arg);
	}

    /**
     * Get start time
	 * @param bool $micro
	 * @return integer|float
     */
    public function startTime($micro = false)
    {
		return $micro ? static::$_start_micro_time : static::$_start_time;
	}
	
    /**
     * Get current time
	 * @return integer
     */
    public function getTime()
    {
		return dateTime()->getTimestamp();
	}
}
