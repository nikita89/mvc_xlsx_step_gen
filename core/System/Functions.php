<?php
/**
 * Autoloader
 * @param string $class
 */
function autoload_handler($class)
{
	$path = ROOT.'/'.str_replace('\\', '/', lcfirst($class)).'.php';
	if (!file_exists($path)) {
		throw new \Exception('Autoload class fail: '.$class);
	}
	require $path;
	if (!class_exists($class) && !trait_exists($class)) {
		throw new \Exception('Autoload class fail: '.$class);
	}
}

/**
 * Application
 * @param string|null $instance
 * @return object
 */
function app($instance = null)
{
	if (!is_null($instance)) {
		return app()->getRegisteredInstanceAlias($instance);
	}
	return \Core\Application::getInstance();
}

/**
 * Session
 * @return Session
 */
function session()
{
	return app()->getRegisteredInstanceAlias('session');
}

/**
 * Request
 * @return Request
 */
function request()
{
	return app()->getRegisteredInstanceAlias('request');
}

/**
 * Data validator
 * @param array $input
 * @param array $rules
 * @return Validator
 */
function validator($input = [], $rules)
{
	return new \Components\Validator($input, $rules);
}

/**
 * File Manager
 * @param string $path
 * @return Manager
 */
function fileManager($path)
{
	return new \Components\Files\Manager($path);
}

/**
 * Logger
 * @param string $name
 * @return Logger
 */
function logger($name = null)
{
    return \Components\Logger::getInstance($name);
}

/**
 * Cache Manager
 * @param string $category
 * @return Cache
 */
function cacheManager($category)
{
	return new \Components\Cache($category);
}

/**
 * Encrypt data
 * @param string $val
 * @return string
 */
function encrypt($val)
{
	return Components\Security\Crypto::encrypt($val);
}

/**
 * Decrypt data
 * @param string $val
 * @return string
 */
function decrypt($val)
{
	return Components\Security\Crypto::decrypt($val);
}

/**
 * Current time
 * @return integer
 */
function getTime()
{
	return app()->getTime();
}

/**
 * Current date
 * @return DateTime
 */
function dateTime()
{
	return new \DateTime('now', new \DateTimeZone(config('timezone')));
}

/**
 * Get formatted date
 * @param mixed $format
 * @return string
 */
function dateFor($format)
{
	return date($format, getTime());
}

/**
 * Get URI
 * @param string $to
 */
function uri($to = '')
{
	return APP_URI.$to;
}

/**
 * Get URL
 * @param string $to
 */
function url($to = '')
{
	return HOST_URL.APP_URI.$to;
}

/**
 * Get Route object
 * @return Route
 */
function route()
{
	return app()->getRegisteredInstanceAlias('route');
}

/**
 * Get route url
 * @param string $name
 * @param array $arg
 */
function urlRoute($name, $arg = [])
{
	return route()->url($name, $arg);
}

/**
 * Route to
 * @param string $name
 * @param array $arg
 */
function to($name, $arg = [])
{
	return route()->to($name, $arg);
}

/**
 * Json encode
 * @param mixed $val
 * @return string
 */
function jsonEncode($val)
{
    return json_encode($val, JSON_UNESCAPED_UNICODE);
}

/**
 * Parse numeric
 * @param mixed $val
 */
function parse_numeric($val)
{
	return \Components\Support\Str::toNumeric($val);
}

/**
 * Config val
 * @param string $key
 * @param mixed $default
 */
function config($key, $default = null)
{
    return \Components\Settings::val($key, $default);
}

/**
 * Email instance
 * @return Email
 */
function email()
{
	return new \Components\Email();
}

/**
 * View display
 * @param string $tpl
 * @param array $data
 */
function view($tpl = 'main.index', $data = ['title' => ''])
{
	$view = new \Core\Views\View;
	$view->tpl($tpl);
	if (strpos($tpl, '.') !== false) {
		$tpl = explode('.', $tpl);
		$view->tpl($tpl[0]);
		$view->contentFile($tpl[1]);
	}
	$view->generate($data);
	\Components\Response::out();
}

/**
 * Out content
 * @param mixed $content
 */
function out($content = null)
{
	\Components\Response::out($content);
}

/**
 * Ajax response
 * @param mixed $response
 */
function ajaxResponse($response = [])
{
	return Components\Response::json($response);
}

/**
 * Ajax errors response
 * @param mixed $msg
 * @param integer $http_code
 * @param mixed $code
 */
function ajaxError($msg = '', $http_code = 200, $code = null)
{
	http_response_code($http_code);
	$resp = [
		'status' => false,
		'error' => [
			'message' => $msg
		]
	];
	if (!is_null($code)) {
		$resp['error']['code'] = $code;
	}
	return Components\Response::json($resp);
}

/**
 * Ajax success response
 * @param mixed $response
 * @param bool $status
 */
function ajaxSuccess($response = [], $status = true)
{
	return Components\Response::json([
		'status' => $status,
		'response' => $response
	]);
}

/**
 * Get collection
 * @param array $items
 * @return Collection
 */
function collection($items = [])
{
    return new \Components\Support\Collection($items);
}

/**
 * Get Events
 * @return Events
 */
function events()
{
    return \Components\Events::getInstance();
}

/**
 * Fire event
 * @param Event $event
 */
function event(\App\Events\Event $event)
{
    return \Components\Events::getInstance()->fire($event);
}

/**
 * Set cookie
 * @param string $name
 * @param string $value
 * @param integer $time in hours
 * @param string $path
 * @return bool
 */
function cookie($name, $value, $time = 0, $path = '/')
{
    if ($time == 0) {
        if (!setcookie($name, $value, 0, $path)) return false;
    } else {
        if (!setcookie($name, $value, time() + ($time * 60), $path)) return false;
    }
    return true;
}

/**
 * Get model class from short name
 * @param string $model_name
 * @return string
 */
function model_class($model_name)
{
	return 'App\\Models\\'.ucfirst($model_name);
}

/**
 * Get Function args
 * @param function $function
 * @return array
 */
function getFunctionArgs($function)
{
    $f = new \ReflectionFunction($function);
    $result = [];
    foreach ($f->getParameters() as $param) {
        $result[] = (object)[
			'name' => $param->getName(),
			'class' => $param->getClass() ? $param->getClass()->name : null,
		];
    }
    return $result;
}

/**
 * Get Method args
 * @param object $object
 * @param string $method
 * @return array
 */
function getMethodArgs($object, $method)
{
    $f = new \ReflectionMethod($object, $method);
    $result = [];
    foreach ($f->getParameters() as $param) {
        $result[] = (object)[
			'name' => $param->getName(),
			'class' => $param->getClass() ? $param->getClass()->name : null,
		];
    }
    return $result;
}

/**
 * Get Function registered args
 * @param callable $function
 * @param array $current_args
 * @return array
 */
function getFunctionRegisteredArgs($function, $current_args = [])
{
	$ask_args = getFunctionArgs($function);
	$args = [];
	foreach ($ask_args as $ask_arg) {
		$value = null;
		if (!empty($ask_arg->class)) {
			$ask_class = $ask_arg->class;
			$object = false;
			foreach ($current_args as $arg) {
				if ($arg instanceof $ask_class) {
					$object = $arg;
				}
			}
			if (!$object) {
				$object = app()->getRegistered($ask_class, $value);
			}
			if (!$object) {
				$not_found_method = 'notFound';
				if (method_exists($ask_class, $not_found_method)) {
					return $ask_class::$not_found_method();
				}
				error(404, $ask_arg->name.' not found');
			}
			$args[$ask_arg->name] = $object;
		} else {
			$args[$ask_arg->name] = $value;
		}
	}
	return $args;
}

/**
 * Get Method registered args
 * @param object $object
 * @param string $method
 * @param array $current_args
 * @return array
 */
function getMethodRegisteredArgs($object, $method, $current_args = [])
{
	$ask_args = getMethodArgs($object, $method);
	$args = [];
	foreach ($ask_args as $ask_arg) {
		$value = null;
		if (!empty($ask_arg->class)) {
			$ask_class = $ask_arg->class;
			$object = false;
			foreach ($current_args as $arg) {
				if ($arg instanceof $ask_class) {
					$object = $arg;
				}
			}
			if (!$object) {
				$object = app()->getRegistered($ask_class, $value);
			}
			if (!$object) {
				$not_found_method = 'notFound';
				if (method_exists($ask_class, $not_found_method)) {
					return $ask_class::$not_found_method();
				}
				error(404, $ask_arg->name.' not found');
			}
			$args[$ask_arg->name] = $object;
		} else {
			$args[$ask_arg->name] = $value;
		}
	}
	return $args;
}

/**
 * Get Method registered args
 * @param string $class
 * @param string $method
 * @param array $current_args
 * @return array
 */
function getStaticMethodRegisteredArgs($class, $method, $current_args = [])
{
	return getMethodRegisteredArgs($class, $method, $current_args);
}

/**
 * Process job
 * @param Job $job
 * @return mixed
 */
function process(\Core\Jobs\Job &$job)
{
	$result = false;
	try {
		$result = call_user_func_array(
			[$job, 'handle'], getMethodRegisteredArgs($job, 'handle')
		);
	} catch(\Exception $e) {
		$job->setException($e);
	}
	return $result;
}

/**
 * Micro sleep
 * @param float $time
 */
function msleep($time)
{
    usleep($time * 1000000);
}

/**
 * UTF-8 aware parse_url()
 * @param string $url
 * @return array
 */
function mb_parse_url($url)
{
	$enc_url = preg_replace_callback('%[^:/@?&=#]+%usD', function ($matches) {
		return urlencode($matches[0]);
	}, $url); 
	$parts = parse_url($enc_url);
	if($parts === false) {
		throw new \InvalidArgumentException('Malformed URL: ' . $url);
	}
	foreach($parts as $name=>$value) {
		$parts[$name] = urldecode($value);
	}
	return $parts;
}

/**
 * Check enabled exec function
 * @return bool
 */
function exec_enabled()
{
	$disabled_functions = explode(',', ini_get('disable_functions'));
	return !in_array(strtolower(ini_get('safe_mode')), array('on', '1'), true) && function_exists('exec') && !in_array('exec', $disabled_functions);
}

/**
 * New error 
 * @param integer $code
 * @param array $trace
 */
function error($code, $trace = []) 
{
	return new \Core\Handlers\Error($code, $trace);
}

/**
 * Custom exception
 * @param Exception $e
 */
function exception_handler($e)
{
	$ex = new \Core\Handlers\Exception($e->getMessage(), $e->getCode(), $e);
	echo $ex->__toString();
}

/**
 * Custom error
 * @param string $m
 */
function error_handler($m, $c, $f, $l)
{
	new \Core\Handlers\Error(0, $c, '['.getErrorType($m).'] '.str_replace(config('path'), '', $f).'('.$l.')');
}

/**
 * Get error type
 * @param string $level
 */
function getErrorType($level) 
{ 
    switch($level) 
    { 
        case E_ERROR: // 1
            return 'E_ERROR'; 
        case E_WARNING: // 2
            return 'E_WARNING'; 
        case E_PARSE: // 4
            return 'E_PARSE'; 
        case E_NOTICE: // 8
            return 'E_NOTICE'; 
        case E_CORE_ERROR: // 16
            return 'E_CORE_ERROR'; 
        case E_CORE_WARNING: // 32
            return 'E_CORE_WARNING'; 
        case E_COMPILE_ERROR: // 64
            return 'E_COMPILE_ERROR'; 
        case E_COMPILE_WARNING: // 128
            return 'E_COMPILE_WARNING'; 
        case E_USER_ERROR: // 256
            return 'E_USER_ERROR'; 
        case E_USER_WARNING: // 512
            return 'E_USER_WARNING'; 
        case E_USER_NOTICE: // 1024
            return 'E_USER_NOTICE'; 
        case E_STRICT: // 2048
            return 'E_STRICT'; 
        case E_RECOVERABLE_ERROR: // 4096
            return 'E_RECOVERABLE_ERROR'; 
	}
}

/**
 * Get Month days
 * @param string $month
 * @param integer $year 
 * @return Collection
 */
function getMonthDays($month = null, $year = null)
{
	if (is_null($month)) {
		$month = dateFor('F');
	}
	if (is_null($year)) {
		$year = dateFor('Y');
	}
	$days = collection([]);
	$oStart = new \DateTime($year.'-'.$month.'-01');
	$oEnd = clone $oStart;
	$oEnd->add(new \DateInterval("P1M"));

	while ($oStart->getTimestamp() < $oEnd->getTimestamp()) {
		$days->push(clone $oStart);
		$oStart->add(new \DateInterval("P1D"));
	}
	return $days;
}

/**
 * First symbol to uppercase
 * @param string $string
 * @param string $enc 
 * @return string
 */
function mb_ucfirst($string, $enc = 'UTF-8')
{
	return mb_strtoupper(mb_substr($string, 0, 1, $enc), $enc).mb_substr($string, 1, mb_strlen($string, $enc), $enc);
}

require ROOT . '/components/Functions/Custom.php';
