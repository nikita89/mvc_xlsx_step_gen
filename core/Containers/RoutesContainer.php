<?php
/**
 * Web Application single bootable container  
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace Core\Containers;
use Core\Traits;

class RoutesContainer
{
	use Traits\Singleton;
	
    /**
     * Constructor
     */
    protected function __construct()
    {
		call_user_func_array(
			[$this, 'sys'], getMethodRegisteredArgs($this, 'sys')
		);			
		if (method_exists($this, 'web')) {
			call_user_func_array(
				[$this, 'web'], getMethodRegisteredArgs($this, 'web')
			);
		}
		if (method_exists($this, 'cli')) {
			call_user_func_array(
				[$this, 'cli'], getMethodRegisteredArgs($this, 'cli')
			);
		}
	}
	
	protected function sys(\Components\Route $route)
	{
		
	}
}
