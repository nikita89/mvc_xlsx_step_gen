<?php
/**
 * Web Application single bootable container  
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace Core\Containers;
use Core\Traits;

class BootableSingle
{
	use Traits\Singleton;
	
    /**
     * Constructor
     */
    protected function __construct()
    {
		call_user_func_array(
			[$this, '_boot'], getMethodRegisteredArgs($this, '_boot')
		);
	}
}
