<?php
/**
 * Web Application Job
 */
namespace Core\Jobs;

class Job 
{
	protected 
		$exception;
	
    /**
     * Has job failed
	 * @return bool
     */
	public function hasFailed()
	{
		return !is_null($this->exception);
	}
	
	/**
	 * Handle job exception
	 * @param Exception $e
	 */
    public function setException(\Exception $e)
    {
		$this->exception = $e;
		if (method_exists($this, 'failed')) {
			$this->failed($this->exception);
		}
    }
	
	/**
	 * Get job exception
	 * @return Exception
	 */
    public function getException()
    {
		return $this->exception;
    }
	
	/**
	 * Process job
	 * @param Job $job
	 * @param bool $force
	 */
    public static function process(Job &$job)
    {
		$result = false;
		try {
			$result = call_user_func_array(
				[$job, 'handle'], getMethodRegisteredArgs($job, 'handle')
			);
		} catch(\Exception $e) {
			$job->setException($e);
		}
		return $result;
	}
}
