<?php
/**
 * Web Application Form class
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace Core\Views;

abstract class Form
{
    /**
     * Get select html
	 * @param mixed $name
	 * @param mixed $values
	 * @param mixed $selected_value
	 * @param mixed $prepend_default
	 * @return string
     */
    public static function select($names, $values, $selected_value = null, $prepend_default = true)
    {
		$param = [
			'id' => '',
			'name' => '',
			'class' => '',
			'required' => false,
			'attributes' => ''
		];
		if (!is_array($names)) {
			$names = ['name' => $names];
		}
		$param = array_merge($param, $names);
		$options = [];
		if ($prepend_default) {
			$options[]= '<option value="">Выбрать</option>';
		}
		foreach ($values as $key=>$value) {
			$selected = '';
			if (!is_null($selected_value)) {
				if (is_array($selected_value) && in_array($key, $selected_value)) {
					$selected .= ' selected';
				} else if($key == $selected_value) {
					$selected .= ' selected';
				}
			}
			$options[]= '<option value="'.$key.'"'.$selected.'>'.$value.'</option>';
		}
		$required = '';
		if ($param['required']) {
			$required = ' required';
		}
		return '<select id="'.$param['id'].'" class="'.$param['class'].'" name="'.$param['name'].'"'.$param['attributes'].$required.'>'.join("\n", $options).'</select>';
    }
	
    /**
     * Get checkbox html
	 * @param mixed $name
	 * @param bool $checked
	 * @return string
     */
    public static function checkbox($name, $checked = false)
    {
		$param = [
			'id' => '',
			'name' => '',
			'class' => '',
			'value' => '',
			'checked' => false,
			'required' => false,
		];
		if (!is_array($name)) {
			$name = ['name' => $name];
		}
		$param = array_merge($param, $name);
		$checked = '';
		if ($param['checked']) {
			$checked = ' checked';
		}
		$required = '';
		if ($param['required']) {
			$required = ' required';
		}
		return '<input type="checkbox" id="'.$param['id'].'" class="'.$param['class'].'" name="'.$param['name'].'" value="'.$param['value'].'"'.$checked . $required.'>';
    }
}
