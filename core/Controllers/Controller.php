<?php
/**
 * Web Application Controller
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace Core\Controllers;

class Controller
{
    protected 
		$request,
		$user,
		$app;

    public function __construct()
    {
		$this->app = app();
		$this->request = request();
    }

    /**
     * Get middleware classes
	 * @param array $class_names Middleware classes
	 * @param mixed $arg arguments
     */
    protected function middleware($class_names = [], $arg = null)
    {
		if (!is_array($class_names)) {
			$class_names = [$class_names];
		}
		foreach ($class_names as $mw_class) {
			
			$pathname = $mw_class;
			$mw_class = 'App\\Middleware\\'.$mw_class;

			$mw_object = new $mw_class($this->request);
			$mw_object->handle($arg);
		}
	}
}
