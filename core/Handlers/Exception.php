<?php
/**
 * MVC Exception
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace Core\Handlers;

class Exception extends \Exception
{
    public function __construct($message, $code = 0, $previous = null)
    {
		parent::__construct($message, $code, $previous);
    }

    public function __toString()
    {
		$error = new Error(1, $this->stringTrace(), 'App Exception', true);
		return $error->__toString();
    }
	
    public function stringTrace()
    {
		$trace = array();
		$stack = array_reverse($this->getTrace());

		if (count($stack == 1) && isset($stack[0]['function']) && $stack[0]['function'] == 'exception_handler') {
			$exception = $stack[0]['args'][0];
			$stack = array_reverse($exception->getTrace());
			$stack[]= array(
				'file' => $exception->getFile(),
				'line' => $exception->getLine(),
			);
		}
		foreach ($stack as $k=>$itm) {

			if(!isset($itm['args'])) {
				$itm['args'] = array();
			}
			if (isset($itm['file'], $itm['line'], $itm['function'], $itm['class'], $itm['type'])) {
				
				$str = str_replace(ROOT, '...', $itm['file']).'('.$itm['line'].'): <b>'.$itm['class'].$itm['type'].$itm['function'].'('.$this->argsToString($itm['args']).')</b>';
			
			} else if (isset($itm['file'], $itm['line'], $itm['class'], $itm['type'])) {
			
				$str = str_replace(ROOT, '...', $itm['file']).'('.$itm['line'].'):';

			} else if (isset($itm['file'], $itm['line'], $itm['function'], $itm['type'])) {
				
				$str = str_replace(ROOT, '...', $itm['file']).'('.$itm['line'].'): <b>'.$itm['function'].'('.$this->argsToString($itm['args']).')</b>';
				
			} else if (isset($itm['class'], $itm['function'], $itm['type'])) {
				
				$str = 'called: <b>'.$itm['class'].$itm['type'].$itm['function'].'('.$this->argsToString($itm['args']).')</b>';
				
			} else if (isset($itm['file'], $itm['line'], $itm['function'])) {
				
				$str = str_replace(ROOT, '...', $itm['file']).'('.$itm['line'].'): <b>'.$itm['function'].'('.$this->argsToString($itm['args']).')</b>';
				
			} else if (isset($itm['file'], $itm['line'])) {
				
				$str = 'in '.str_replace(ROOT, '...', $itm['file']).'('.$itm['line'].'):';
				
			} else if (isset($itm['function'])) {
				
				$str = 'called: <b>'.$itm['function'].'('.$this->argsToString($itm['args']).')</b>';
				
			} else {
				$str = $str = '<b>-</b>';
			}
			$trace[]= '<div class="line">['.$k.'] '.$str."</div>";
		}
		$trace[]= '<div class="excMsg">'.$this->getMessage().'</div>';

		return "\r\n".implode("\r\n", $trace);
	}
	
    protected function argsToString($args = array())
    {
		$arg_list = array();
		foreach ($args as $arg) {
			$type = gettype($arg);
			if (!is_string($arg) && !is_numeric($arg) && !is_double($arg) && !is_bool($arg) && !is_int($arg) && !is_null($arg)) {
				$argument = ucfirst($type);
				if (is_array($arg)) {
					$argument .= '['.count($arg).']';
				}
			} else {
				$argument = '<i>' . $type . "</i> '" . str_replace(ROOT, '...', (string)$arg)."'";
			}
			$arg_list[]= $argument;
		}
		return '<span class="args">'.implode(', ', $arg_list).'</span>';
	}
}
