<?php
/**
 * Web Application Server errors
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace Core\Handlers;
use Components\Support\Str,
	Components\Response;

class Error
{
	private $tpl,
			$msg,
			$title,
			$code = 0,
			$titles = array(
				  0 => 'App Error',
				  1 => 'App Exception',
				400 => 'Bad Request',
				401 => 'Unauthorized',
				403 => 'Forbidden',
				404 => 'Not Found',
				500 => 'Internal Server Error',
				503 => 'Service Unavailable'
			);
			
    public function __construct($code = null, $msg = null, $title = null, $ret = false)
    {
		$this->code = (int)$code;
		$this->msg = Str::toString($msg);
		$this->title = $this->getTitle($this->code);
		if (!is_null($title)) {
			$this->title = Str::toString($title);
		}
		$error_tpl = 'templates/error/'.$this->code.'.html';
		if (!file_exists(RESOURCES.'/'.$error_tpl)) {
			$error_tpl = 'templates/error/page.html';
		}
		$this->tpl = file_get_contents(RESOURCES.'/'.$error_tpl);
		error_log($this->title.': '.strip_tags($this->msg));
		if (!$ret) {
			$this->render();
		}
	}
	
    /**
     * Error title from
	 * @param integer $code Error code
	 * @return string title
     */
    public function getTitle( $code )
    {
		if (!isset($this->titles[$code])) {
			return 'Untitled error';
		}
		return $this->titles[$code];
	}
	
    /**
     * Error to string
     */
    public function __toString()
    {
		return strtr(
			$this->tpl, [
				'{title}' => $this->title,
				'{code}' => $this->code,
				'{msg}' => $this->msg
			]	
		);
	}
	
    /**
     * Error page
     */
    public function render()
    {
		http_response_code($this->code);
		Response::html(
			$this->__toString()
		);
	}
}
