<?php
/**
 * Web Application Model - Коллекиця связанных младших моделей
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace Core\Models\Collections;

class HasLinked extends Model
{
	/**
     * Constructor
	 * @param array $elements
	 * @param string $target class
	 * @param string $model object
	 * @param mixed $linked_col столбец связи
     */
    public function __construct(Array $elements = [], $target = null, $model = null, $linked_col = null)
    {
        parent::__construct($elements);
		
		if (!is_null($target) && !is_null($model)) {
			
			if (!($model instanceof \Core\Models\DbModel)) {
				throw new \Exception('Model must be instance of DbModel');
			}
			$wrapper = "Core\\Models\\Wrappers\\".substr(strrchr(static::class, "\\"), 1);
			if (!class_exists($wrapper)) {
				throw new \Exception('Collection wrapper not found: '.$wrapper);
			}
			$this->wrapper = new $wrapper($this, $target, $model, $linked_col);			
		}
	}
}
