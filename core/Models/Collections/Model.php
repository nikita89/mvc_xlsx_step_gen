<?php
/**
 * Web Application Model Collection class
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace Core\Models\Collections;

class Model extends \Components\Support\Collection
{
	protected $wrapper;
	
    /**
     * Constructor
	 * @param array $elements
	 * @param string $target class
	 * @param string $model object
     */
    public function __construct(Array $elements = [], $target = null, $model = null)
    {
        parent::__construct($elements);
		
		if (!is_null($target) && !is_null($model)) {
			
			if (!($model instanceof \Core\Models\DbModel)) {
				throw new \Exception('Model must be instance of DbModel');
			}
			$wrapper = "Core\\Models\\Wrappers\\".substr(strrchr(static::class, "\\"), 1);
			if (!class_exists($wrapper)) {
				throw new \Exception('Collection wrapper not found: '.$wrapper);
			}
			$this->wrapper = new $wrapper($this, $target, $model);			
		}
	}
	
    /**
     * Get array data from models
	 * @return array
     */
    public function toArray()
    {
		$this->each(function(&$item) {
			if ($item instanceof \Core\Models\DbModel) {
				$item = $item->toArray();
			}
		});
		return $this->all();
	}
	
    /**
     * Get wrapper
	 * @return Wrapper
     */
    public function wrapper()
    {
		return $this->wrapper;
	}
	
    /**
     * Remove models
	 * @param mixed $key
	 * @return bool
     */
	public function remove($key = null)
	{
		$this->each(function($item) {
			$item->remove();
		});
	}
}
