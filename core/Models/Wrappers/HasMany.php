<?php
/**
 * Web Application Wrapper - Обертка младших моделей
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace Core\Models\Wrappers;
use Components\Database\Connection as Db,
	Components\Database\Buffer;

class HasMany extends Model
{
	protected 
		$linked_fields;
		
    /**
     * Constructor
	 * @param Collection $elements
	 * @param string $target class
	 * @param object $model object
	 * @param array $fields additional
     */
    public function __construct($collection, $target, $model, $fields)
    {
        $this->collection = $collection;
		$this->target = $target;
		$this->model = $model;
		$this->linked_fields = $fields;
	}
	
    /**
     * Создание младших сущностей с привязкой к старшей
	 * @param array $data
	 * @return Model
     */
	public function create(Array $data = [])
	{
		$current = get_class($this->model);
		$target = $this->target;
		
		$object = $target::create($data);
		$this->attach($object);
		return $object;
	}
	
    /**
     * Создание связей между сущностями
	 * @param mixed $increments
	 * @return Wrapper
     */
	public function attach($increments, $linked_data = [])
	{
		$current = get_class($this->model);
		$target = $this->target;

		$attaches = [];
		$target_models = $this->getTargetModelsArray($increments);
		$increment = $this->getIncrementsArray($increments);
		
		if (empty($target_models)) {
			throw new \Exception('Error attach models (target models: is empty or unsupported)');
		}
		if (empty($increment)) {
			throw new \Exception('Error attach models (target increment: is empty or unsupported)');
		}
		$linkeds = [];
		if (!empty($linked_data)) {
			foreach ($linked_data as $field=>$val) {
				if (in_array($field, $this->linked_fields)) {
					$linkeds[$field] = $val;
				}
			}
		}
		$attach_fields = [
			'link_id', $current::linkedColName(), $target::linkedColName()
		];
		foreach ($linkeds as $field=>$val) {
			$attach_fields[]= $field;
		}
		foreach ($increment as $attach_id) {
			$attach_values = [
				'"'.md5($this->model->{$current::incrColName()}.'_'.$attach_id).'"',
				$this->model->{$current::incrColName()},
				$attach_id
			];
			foreach ($linkeds as $field=>$val) {
				$attach_values[]= $val;
			}
			$attaches[]= '('.implode(',', $attach_values).')';
		}
		$sql = 'INSERT IGNORE INTO '.$current::hasManyTableName($target).' ('.join(', ', $attach_fields).') VALUES '.join(', ', $attaches);
		Db::link()->rawQuery($sql);
		
		if (Db::link()->getLastErrno() === 0) {
			Buffer::remove($current::hasManyKey($target, $this->model->{$current::incrColName()}));
			foreach ($target_models as $model) {
				Buffer::remove($target::BelongsToManyKey($current, $model->{$model::incrColName()}));
			}
			return $this;
		}
		throw new \Exception('Error attach models: '.Db::link()->getLastError());
	}
	
    /**
     * Разрыв связей между сущностями
	 * @param mixed $increments
	 * @return Wrapper
     */
	public function detach($increments = null)
	{
		$current = get_class($this->model);
		$target = $this->target;
		$target_models = $this->getTargetModelsArray($increments);

		Db::link()->where($current::linkedColName(), $this->model->{$current::incrColName()});
		if (!is_null($increments)) {
			$whereIds = $this->getIncrementsArray($increments);
			if (empty($whereIds)) {
				throw new \Exception('Error detach: empty target ids');
			}
			Db::link()->where($target::linkedColName(), $whereIds, 'IN');
		}
		Db::link()->delete($current::hasManyTableName($target));
		if (Db::link()->getLastErrno() === 0) {
			Buffer::remove($current::hasManyKey($target, $this->model->{$current::incrColName()}));
			foreach ($target_models as $model) {
				Buffer::remove($target::BelongsToManyKey($current, $model->{$model::incrColName()}));
			}
			return $this;
		}
		throw new \Exception('Error detach models: '.Db::link()->getLastError());
	}
}
