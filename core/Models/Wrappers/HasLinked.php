<?php
/**
 * Web Application Wrapper - Обертка связанных младших моделей
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace Core\Models\Wrappers;
use Components\Database\Connection as Db,
	Components\Database\Buffer;

class HasLinked extends Model
{
	protected 
		$linked_col;
		
    /**
     * Constructor
	 * @param Collection $elements
	 * @param string $target class
	 * @param object $model object
	 * @param mixed $linked_col столбец связи
     */
    public function __construct($collection, $target, $model, $linked_col = null)
    {
        $this->collection = $collection;
		$this->target = $target;
		$this->model = $model;
		
		if (!is_string($linked_col)) {
			$model = get_class($this->model);
			$linked_col = $model::linkedColName();
		}
		$this->linked_col = $linked_col;
	}
	
    /**
     * Создание младших сущностей с привязкой к старшей
	 * @param array $data
	 * @return Model
     */
	public function insert(Array $data = [])
	{
		$current = get_class($this->model);
		$target = $this->target;
		
		foreach ($data as &$item) {
			$item[$this->linked_col] = $this->model->{$current::incrColName()};
		}
		$models = $target::insert($data);
		Buffer::remove($current::hasLinkedKey($target, $this->model->{$current::incrColName()}));		
		return $models;
	}
	
    /**
     * Создание младшей сущности с привязкой к старшей
	 * @param array $data
	 * @return Model
     */
	public function create(Array $data = [])
	{
		$current = get_class($this->model);
		$target = $this->target;
		
		$data[$this->linked_col] = $this->model->{$current::incrColName()};
		$model = $target::create($data);

		Buffer::remove($current::hasLinkedKey($target, $this->model->{$current::incrColName()}));
		return $model;
	}
	
    /**
     * Обновление младших сущностей
	 * @param mixed $increments
	 * @return Wrapper
     */
	public function update($update = [])
	{
		$current = get_class($this->model);
		$target = $this->target;
		
		$emptyModel = new $target([]);
		$updateFields = collection(array_keys($update));
		$writableFields = $emptyModel->writableFields();
		$update['modified_at'] = date('Y-m-d H:i:s', getTime());
		
		$failed = $updateFields->filter(function($value, $key) use($writableFields) {
			return !in_array($value, $writableFields);
		});
		if ($failed->count() > 0) {
			throw new \Exception('Non-writable model fields detected: '.$failed->join(', '));
		}
		Db::link()->where($this->linked_col, $this->model->{$current::incrColName()})
				  ->update($target::getTableName(), $update);
		
		if (Db::link()->getLastErrno() === 0) {
			Buffer::remove($current::hasLinkedKey($target, $this->model->{$current::incrColName()}));
			return $this;
		}
		throw new \Exception('Error update models: '.Db::link()->getLastError());
	}
	
    /**
     * Удаление младших сущностей
	 * @param mixed $increments
	 * @return Wrapper
     */
	public function remove($increments = null)
	{
		$current = get_class($this->model);
		$target = $this->target;

		Db::link()->where($this->linked_col, $this->model->{$current::incrColName()});
		if (!is_null($increments)) {
			$target_increments = $this->getIncrementsArray($increments);
			Db::link()->where($target::incrColName(), $target_increments, 'IN');
		}
		Db::link()->delete($target::getTableName());

		if (Db::link()->getLastErrno() === 0) {
			Buffer::remove($current::hasLinkedKey($target, $this->model->{$current::incrColName()}));
			return $this;
		}
		throw new \Exception('Error remove models: '.Db::link()->getLastError());
	}
	
    /**
     * Привязка младших сущностей к старшей
	 * @param mixed $increments
	 * @return Wrapper
     */
	public function attach($increments)
	{
		$current = get_class($this->model);
		$target = $this->target;
		$target_models = $this->getTargetModelsArray($increments);
		
		$update = [];
		$update[$this->linked_col] = $this->model->{$current::incrColName()};
		$target_increments = $this->getIncrementsArray($increments);

		Db::link()->where($target::incrColName(), $target_increments, 'IN')
				  ->update($target::getTableName(), $update);
				  
		if (Db::link()->getLastErrno() === 0) {
			Buffer::remove($current::hasLinkedKey($target, $this->model->{$current::incrColName()}));
			foreach ($target_models as &$model) {
				$model->{$this->linked_col} = $this->model->{$current::incrColName()};
			}
			return $this;
		}
		throw new \Exception('Error attach models: '.Db::link()->getLastError());
	}
	
    /**
     * Отвязка младших сущностей
	 * @param mixed $increments
	 * @return Wrapper
     */
	public function detach($increments = null)
	{
		$current = get_class($this->model);
		$target = $this->target;
		$target_models = [];
		
		$update = [];
		$update[$this->linked_col] = null;
		
		Db::link()->where($this->linked_col, $this->model->{$current::incrColName()});
		if (!is_null($increments)) {
			$target_models = $this->getTargetModelsArray($increments);
			$target_increments = $this->getIncrementsArray($increments);
			Db::link()->where($target::incrColName(), $target_increments, 'IN');
		}
		Db::link()->update($target::getTableName(), $update);

		if (Db::link()->getLastErrno() === 0) {
			Buffer::remove($current::hasLinkedKey($target, $this->model->{$current::incrColName()}));
			foreach ($target_models as &$model) {
				$model->{$this->linked_col} = null;
			}
			return $this;
		}
		throw new \Exception('Error detach models: '.Db::link()->getLastError());
	}
}
