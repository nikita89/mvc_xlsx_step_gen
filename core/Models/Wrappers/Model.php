<?php
/**
 * Web Application Wrapper - Обертка моделей
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace Core\Models\Wrappers;
use Components\Database\Connection as Db,
	Components\Database\Buffer;

class Model
{
	use Traits\Support;
	
	protected $model,
			  $target,
			  $collection;
    /**
     * Constructor
	 * @param Collection $elements
	 * @param string $target class
	 * @param object $model object
     */
    public function __construct($collection, $target, $model)
    {
        $this->collection = $collection;
		$this->target = $target;
		$this->model = $model;
	}

    /**
     * Get model collection
	 * @return Collections\Model
     */
    public function resource()
    {
		return $this->collection;
	}
}
