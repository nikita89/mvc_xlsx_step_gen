<?php
/**
 * Web Application Collection trait
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace Core\Models\Wrappers\Traits;

trait Support
{
    /**
     * Get increments array from mixed
	 * @param Models|array|integer|string $increments
     * @return array
     */
    public static function getIncrementsArray($increments)
    {
		if (empty($increments)) {
			return [];
		}
		// one numeric || string
		if (is_numeric($increments) || is_string($increments)) {
			return [$increments];
		}
		// collection
		if ($increments instanceof \Core\Models\Collections\Model) {
			$values = [];
			foreach ($increments->all() as $model) {
				$values[]= $model->getIncrement();
			}
			return $values;
		}
		// model
		if ($increments instanceof \Core\Models\DbModel) {
			return [$increments->getIncrement()];
		}
		// array
		if (is_array($increments)) {
			$items = array_values($increments);
			$values = [];
			// models
			if ($items[0] instanceof \Core\Models\DbModel) {
				foreach ($items as $model) {
					$values[]= $model->getIncrement();
				}
			// numeric || string
			} else if (is_numeric($items[0]) || is_string($items[0])) {
					$values = $items;
			}
			return $values;
		}
		return [];
    }
	
    /**
     * Get models array from mixed
	 * @param Model|array $models
     * @return array
     */
    public static function getTargetModelsArray($models)
    {
		// collection
		if ($models instanceof \Core\Models\Collections\Model) {
			return $models->all();
		}
		// model
		if ($models instanceof \Core\Models\DbModel) {
			return [$models];
		}
		// array
		if (is_array($models)) {
			$items = array_values($models);
			// models
			if ($items[0] instanceof \Core\Models\DbModel) {
				return $items;
			}
		}
		return [];
	}
}
