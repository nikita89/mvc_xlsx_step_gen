<?php
/**
 * Web Application Wrapper - Обертка старшей модели
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace Core\Models\Wrappers;
use Components\Database\Connection as Db,
	Components\Database\Buffer;

class BelongsTo extends Model
{
	protected 
		$linked_col;
	
	/**
     * Constructor
	 * @param Collection $elements
	 * @param string $target class
	 * @param object $model object
	 * @param mixed $linked_col столбец связи
     */
    public function __construct($collection, $target, $model, $linked_col = null)
    {
        $this->collection = $collection;
		$this->target = $target;
		$this->model = $model;
		if (!is_string($linked_col)) {
			$target_class = get_class($target);
			$linked_col = $target_class::linkedColName();
		}
		$this->linked_col = $linked_col;
	}
	
    /**
     * Создание старших сущностей с привязкой к старшей
	 * @param array $data
	 * @return Model
     */
	public function create(Array $data = [])
	{
		$current = get_class($this->model);
		$target = $this->target;
		
		$object = $target::create($data);
		$this->model->{$this->linked_col} = $object->{$object::incrColName()};
		$this->model->save();
		return $object;
	}
	
    /**
     * Привязка старшей сущности к младшей
	 * @param Model $parent
	 * @return Wrapper
     */
	public function attach($parent)
	{
		$current = get_class($this->model);
		$target = $this->target;
		if (!($parent instanceof \Core\Models\DbModel)) {
			throw new \Exception('Attach argument must me DbModel');
		}
		$this->model->{$this->linked_col} = $parent->{$parent::incrColName()};
		$this->model->save();

		Buffer::remove($target::hasLinkedKey($current, $parent->{$parent::incrColName()}));
		return $this;
	}
	
    /**
     * Отвязвка старшей сущности от младшей
	 * @return Wrapper
     */
	public function detach()
	{
		$current = get_class($this->model);
		$target = $this->target;
		$parent = $this->resource();
		
		$this->model->{$this->linked_col} = null;
		$this->model->save();

		if ($parent instanceof \Core\Models\DbModel) {
			Buffer::remove($target::hasLinkedKey($current, $parent->{$parent::incrColName()}));
		}
		return $this;
	}
	
    /**
     * Отвязвка старшей сущности через младшую
	 * @return Wrapper
     */
	public function remove()
	{
		$current = get_class($this->model);
		$target = $this->target;
		$parent = $this->model->belongsTo($target)->resource();
		$this->detach();
		if ($parent instanceof \Core\Models\DbModel) {
			$parent->remove();
		}
		return $this;
	}
	
    /**
     * Get model object
	 * @return Collections\Model
     */
    public function resource()
    {
		return $this->collection->get(0);
	}
}
