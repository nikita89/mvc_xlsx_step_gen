<?php
/**
 * Web Application Model
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace Core\Models;

class Model
{
	use Traits\Detector, Traits\Converter;
	
	protected 
		$system = [],
		$hidden = [],
		$writable = [],
		$attributes = [],
		$changed = [];
		
    /**
     * Constructor
	 * @param mixed $data
     */
    public function __construct($data = [])
    {
		if (!is_array($data) && !is_object($data)) {
			throw new \Exception(static::getBasename().' model data must be array or object');
		}
        foreach ($this->system as $field_key) {
			$this->attributes[$field_key] = null;
		}
        foreach ($this->hidden as $field_key) {
			$this->attributes[$field_key] = null;
		}
        foreach ($this->writable as $field_key) {
			$this->attributes[$field_key] = null;
		}
        foreach ($data as $field_key=>$val) {
			if (array_key_exists($field_key, $this->attributes)) {
				$this->attributes[$field_key] = $val;
			}
		}
		$this->_boot();
    }
	
    /**
     * Model on load
     */
    protected function _boot()
    {
		// code...
	}
	
    /**
     * Has model attribute
	 * @param string $field_key
	 * @return bool
     */
	public function hasAttribute($field_key)
	{
		return array_key_exists($field_key, $this->attributes);
	}
	
	/**
     * Get model changed fields
	 * @return array
     */
    public function changedFields()
    {
		return $this->changed;
	}
	
	/**
     * Has changed field
	 * @param string $field
	 * @return bool
     */
    public function hasChanged($field)
    {
		return in_array($field, $this->changed);
	}
	
    /**
     * Get changed model data
	 * @return array
     */
    public function getChangedData()
    {
		$data = [];
		$changed_fields = $this->changedFields();
		foreach ($changed_fields as $field) {
			$data[$field] = $this->{$field};
		}
		return $data;
	}
	
    /**
     * Get model writable fields
	 * @return array
     */
	public function writableFields()
	{
		return array_merge($this->writable, $this->hidden);
	}
	
    /**
     * Get write fields data
     * @return array
     */
    protected function _getWriteAttributes()
    {
		$fields = [];
		$writable = $this->writableFields();
        foreach ($this->attributes as $key=>$val) {
			if (!in_array($key, $writable)) {
				continue;
			}
			$fields[$key] = $val;
		}
		return $fields;
    }
	
	/**
     * Protect get model fields
	 * @param string $field
     */
	public function __get($field)
	{
		if (!array_key_exists($field, $this->attributes)) {
			
			if (method_exists($this, $field)) {
				
				$result = $this->$field();
				if ($result instanceof \Core\Models\Wrappers\Model) {
					return $result->resource();
				}
				return $result;
			}
			throw new \Exception('Invalid '.static::getBasename().' field: '.$field);
		}
		if ($field == 'linked') {
			if (!($this->attributes['linked'] instanceOf \Core\Models\LinkedModel)) {
				throw new Exception('Linked fields must be called in LinkedModel context only');
			}
		}
		$access_method_name = $field.'_access';
		if (method_exists($this, $access_method_name)) {
			return $this->$access_method_name($this->attributes[$field]);
		}
		return $this->attributes[$field];
	}

    /**
     * Protect set model fields
	 * @param string $property
	 * @param string $value
     */
	public function __set($field, $value)
	{
		if (!array_key_exists($field, $this->attributes)) {
			throw new \Exception('Invalid '.static::getBasename().' field: '.$field);
		}
		if (!in_array($field, $this->writable)) {
			throw new \Exception('Protected '.static::getBasename().' field set fail: '.$field);
		}
		if ($this->attributes[$field] !== $value && !in_array($field, $this->changed)) {
			$this->changed[]= $field;
		}
		$protect_method_name = $field.'_protect';
		if (method_exists($this, $protect_method_name)) {
			if ($this->$protect_method_name($value) === false) {
				return false;
			}
		}
		$this->attributes[$field] = $value;
	}
}
