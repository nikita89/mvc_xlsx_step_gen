<?php
/**
 * Web Application Model trait
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace Core\Models\Traits;

trait Converter
{
    /**
     * Convert Model to array
     * @return array
     */
    public function toArray()
    {
		$fields = [];
		foreach ($this->attributes as $field_key=>$val) {
			if (in_array($field_key, $this->hidden)) {
				continue;
			}
			$fields[$field_key] = $val;
		}
		return $fields;
    }
	
    /**
     * Convert Model to object
     * @return array
     */
    public function toObject()
    {
		return (object)$this->toArray();
    }
	
    /**
     * Convert Model to json
     * @return array
     */
    public function toJson()
    {
		return jsonEncode($this->toArray());
    }
}
