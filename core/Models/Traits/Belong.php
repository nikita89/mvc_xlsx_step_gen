<?php
/**
 * Web Application Model trait
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace Core\Models\Traits;
use Components\Database\Buffer,
	Core\Models\Collections,
	Core\Models\Wrappers,
	Core\Models\LinkedModel,
	Components\Database\Connection as Db;

trait Belong
{
    /**
     * Получить старшие модели - многие ко многим
	 * @param string $parent класс старшей модели
	 * @param arrray $linked_fields - получаем поля из таблицы связи
	 * @return Collection
     */
	public function belongsToMany($parent, $linked_fields = [])
	{
		$buffer_key = static::belongsToManyKey($parent, $this->{$this::incrColName()});
		if (Buffer::has($buffer_key)) {
			return Buffer::get($buffer_key);
		}
		$models = [];
		$selected = Db::link()->join(static::belongsToManyTableName($parent).' m', 'p.'.$parent::incrColName().'=m.'.$parent::linkedColName(), 'LEFT')
							  ->where('m.'.static::linkedColName(), $this->{$this::incrColName()})
							  ->fromTable($parent::getTableName().' p')
							  ->get();
        if ($selected) {
			foreach ($selected as $data) {
				$linked_values = ['link_id' => $data['link_id']];
				foreach ($linked_fields as $key) {
					$linked_values[$key] = isset($data[$key]) ? $data[$key] : null;
				}	
				$data['linked'] = new LinkedModel($linked_values, $linked_fields, static::belongsToManyTableName($parent));
				$models[]= new $parent($data);
			}
        }
		$collection = new Collections\BelongsToMany($models, $parent, $this, $linked_fields);
		$wrapper = $collection->wrapper();

		Buffer::set($buffer_key, $wrapper);
        return $wrapper;
	}
	
    /**
     * Получить связанную старшую модель
	 * @param string $parent класс старшей модели
	 * @param mixed $linked_col столбец связи
	 * @return Model
     */
	public function belongsTo($parent, $linked_col = null)
	{
		if (!is_string($linked_col)) {
			$linked_col = $parent::linkedColName();
		}
		$parent_id = $this->{$linked_col};
		$buffer_key = static::belongsToKey($parent, $this->{$this::incrColName()}, $parent_id);
		if ($parent_id && Buffer::has($buffer_key)) {
			return Buffer::get($buffer_key);
		}
		$model = null;
		$data = Db::link()->where($parent::incrColName(), $parent_id)
						  ->fromTable($parent::getTableName())
						  ->getOne();
        if ($data) {
			$model = new $parent($data);
        }
		$collection = new Collections\BelongsTo([$model], $parent, $this, $linked_col);
		$wrapper = $collection->wrapper();
		if ($parent_id) {
			Buffer::set($buffer_key, $wrapper);
		}
		return $wrapper;
	}
}
