<?php
/**
 * Web Application Model trait
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace Core\Models\Traits;
use Components\Database\Buffer,
	Core\Models\Collections,
	Core\Models\Wrappers,
	Core\Models\LinkedModel,
	Components\Database\Connection as Db;

trait Has
{
    /**
     * Получить младшие модели - многие ко многим
	 * @param string $target класс младшей модели
	 * @param arrray $linked_fields - получаем поля из таблицы связи
	 * @return Wrapper
     */
	public function hasMany($target, $linked_fields = [])
	{
		$buffer_key = static::hasManyKey($target, $this->{$this::incrColName()});
		if (Buffer::has($buffer_key)) {
			return Buffer::get($buffer_key);
		}
		$models = [];
		$selected = Db::link()->join(static::hasManyTableName($target).' m', 't.'.$target::incrColName().'=m.'.$target::linkedColName(), 'LEFT')
							  ->where('m.'.static::linkedColName(), $this->{$this::incrColName()})
							  ->fromTable($target::getTableName().' t')
							  ->get();
        if ($selected) {
			foreach ($selected as $data) {
				$linked_values = ['link_id' => $data['link_id']];
				foreach ($linked_fields as $key) {
					$linked_values[$key] = isset($data[$key]) ? $data[$key] : null;
				}	
				$data['linked'] = new LinkedModel($linked_values, $linked_fields, static::hasManyTableName($target));
				$models[]= new $target($data);
			}
        }
		$collection = new Collections\HasMany($models, $target, $this, $linked_fields);
		$wrapper = $collection->wrapper();

		Buffer::set($buffer_key, $wrapper);
        return $wrapper;
	}
	
    /**
     * Получить связанные младшие модели
	 * @param string $target класс младшей модели
	 * @param mixed $linked_col столбец связи
	 * @return HasLinkedCollection
     */
	public function hasLinked($target, $linked_col = null)
	{
		if (!is_string($linked_col)) {
			$linked_col = static::linkedColName();
		}
		$buffer_key = static::hasLinkedKey($target, $this->{$this::incrColName()});
		if (Buffer::has($buffer_key)) {
			return Buffer::get($buffer_key);
		}
		$models = [];
		$selected = Db::link()->where($linked_col, $this->{$this::incrColName()})
							  ->fromTable($target::getTableName())
							  ->get();
        if ($selected) {
			foreach ($selected as $data) {
				$object = new $target($data);
				$models[]= $object;
			}
        }
		$collection = new Collections\HasLinked($models, $target, $this, $linked_col);
		$wrapper = $collection->wrapper();

		Buffer::set($buffer_key, $wrapper);
		return $wrapper;
	}
	
    /**
     * Получить связанную младшую модель
	 * @param string $target класс младшей модели
	 * @param mixed $linked_col столбец связи
	 * @return Model
     */
	public function hasOne($target, $linked_col = null)
	{
		if (!is_string($linked_col)) {
			$linked_col = static::linkedColName();
		}
		$buffer_key = static::hasLinkedKey($target, $this->{$this::incrColName()});
		if (Buffer::has($buffer_key)) {
			return Buffer::get($buffer_key);
		}
		$models = [];
		$selected = Db::link()->where($linked_col, $this->{$this::incrColName()})
							  ->fromTable($target::getTableName())
							  ->get();
        if ($selected) {
			foreach ($selected as $data) {
				$object = new $target($data);
				$models[]= $object;
			}
        }
		$collection = new Collections\HasOne($models, $target, $this, $linked_col);
		$wrapper = $collection->wrapper();

		Buffer::set($buffer_key, $wrapper);
		return $wrapper;
	}
}
