<?php
/**
 * Web Application Model trait
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace Core\Models\Traits;

trait SqlBuffer
{
    /**
     * Get Sql Buffer key
	 * @param $target class
     * @return string
     */
    public static function hasManyKey($target, $current_id)
    {
		return static::getBasename() . '_' . $current_id . '_hasMany_' . $target::getBasename();
    }
	
    /**
     * Get Sql Buffer key
	 * @param $parent class
     * @return string
     */
    public static function belongsToManyKey($parent, $parent_id)
    {
		return static::getBasename() . '_' . $parent_id . '_belongsToMany_' . $parent::getBasename();
    }
	
    /**
     * Get Sql Buffer key
	 * @param $target class
     * @return string
     */
    public static function hasLinkedKey($target, $current_id)
    {
		return static::getBasename() . '_' . $current_id . '_hasLinked_' . $target::getBasename();
    }
	
    /**
     * Get Sql Buffer key
	 * @param $parent class
     * @return string
     */
    public static function belongsToKey($parent, $current_id, $parent_id)
    {
		return static::getBasename() . '_' . $current_id . '_belongsTo_' . $parent::getBasename() . '_' . $parent_id;
    }
}
