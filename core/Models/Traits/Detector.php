<?php
/**
 * Web Application Model trait
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace Core\Models\Traits;

trait Detector
{
    public static function getBasename()
    {
        return mb_strtolower(substr(static::class, strrpos(static::class, '\\') + 1));
    }
	
    public static function eventsNamespace()
    {
        return str_replace('_', '.', static::getTableName());
    }
}
