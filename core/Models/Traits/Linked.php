<?php
/**
 * Web Application Model trait
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace Core\Models\Traits;

trait Linked
{
    /**
     * Convert Model to array
     * @return array
     */
    public function toArray()
    {
		$data = parent::toArray();
		if ($data['linked'] instanceOf \Core\Models\LinkedModel){
			$data['linked'] = $data['linked']->toArray();
			unset($data['linked']['link_id']);
		} else {
			unset($data['linked']);
		}
		return $data;
    }
}
