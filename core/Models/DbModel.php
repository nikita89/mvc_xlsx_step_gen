<?php
/**
 * Web Application DB Model
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace Core\Models;
use Components\Database\Buffer,
	Components\Database\Connection as Db,
	Core\Models\Collections;

class DbModel extends Model
{
	use Traits\DbTable, Traits\Detector, Traits\Converter, Traits\SqlBuffer;
	
	protected static 
		$_linked_column = null,
		$_table_name = null,
		$_has_many_prefix = null,
		$_belongs_to_many_suffix = null,
		$_ai = true,
		$_incr = 'id';
	protected 
		$system = [
			'id', 'created_at', 'modified_at'
		];
	
    /**
     * Get model increment
	 * @return integer|null
     */
	public function getIncrement()
	{
		if (array_key_exists(static::incrColName(), $this->attributes)) {
			return $this->attributes[static::incrColName()];
		}
		return null;
	}
	
    /**
     * Get models by increment
	 * @param mixed $increment_id
	 * @return Model|null
     */
	public static function find($increment_id = null)
	{
		$options = [];
        if (is_null($increment_id)) {
            return null;
        }
		if (is_array($increment_id)) {
			$options[static::incrColName()] = [$increment_id, 'IN'];
			return static::get($options);
		}
		$options[static::incrColName()] = $increment_id;
		$collection = static::get($options);
		
		if ($collection->count() == 0) {
			return null;
		}
		return $collection->get(0);
	}
	
	
    /**
     * Get models by options
	 * @param array $options Model query options
	 * @return Collection|null
     */
	public static function get($options = [])
	{
        if (!is_array($options)) {
            return null;
        }
		$models = [];
		foreach ($options as $field=>$val) {
			if (is_array($val)) {
				Db::link()->where($field, $val[0], $val[1]);
			} else {
				Db::link()->where($field, $val);
			}
		}
		$selected = Db::link()->fromTable(static::getTableName())
							  ->get();
        if ($selected) {
			foreach ($selected as $data) {
				$object = new static($data);
				$models[]= $object;
			}
        }
        return new Collections\Model($models, static::class);
	}
	
    /**
     * Create model
	 * @param array $data Model data
	 * @return Model
     */
	public static function create($data = [])
	{
		$inserted = static::insert($data);
		return $inserted->get(0);
	}
	
    /**
     * Get or create model
	 * @param mixed $target_values
	 * @return Model
     */
	public static function getCreate($target_values)
	{
		$current = new static;
		if (is_array($target_values) || is_object($target_values)) {
			$get_data = [];
			$create_data = [];
			foreach ($target_values as $field_key=>$field_val) {
				if ($current->hasAttribute($field_key)) {
					$create_data[$field_key] = $field_val;
					if ($field_key == static::incrColName()) {
						$get_data[static::incrColName()] = $field_val;
					}
				}
			}
		} else {
			$data = [];
			$data[static::incrColName()] = $target_values;
			$get_data = $data;
			$create_data = $data;
		}
		if ($model = static::get($get_data)->first()) {
			return $model;
		}
		return static::create($create_data);
	}
	
    /**
     * Insert models to DB
	 * @param array $models data
	 * @return Collection
     */
	public static function insert($models_data = [])
	{
		if (is_object($models_data)) {
			$models_data = (array)$models_data;
		}
		if (!isset($models_data[0]) || !is_array($models_data[0])) {
			$models_data = [$models_data];
		}
		$models_data = array_values($models_data);
		if (static::$_ai === false) {
			return static::insertNoIncrement($models_data);
		}
		$emptyModel = new static([]);
		foreach ($models_data as &$data) {
			
			foreach ($data as $field_key=>$field_val) {
				if (!in_array($field_key, $emptyModel->writableFields())) {
					unset($data[$field_key]);
				}
			}
			$data['created_at'] = dateFor('Y-m-d H:i:s');
			$data['modified_at'] = dateFor('Y-m-d H:i:s');
		}
		$inserted_ids = Db::link()->insertMulti(static::getTableName(), $models_data);
		if (empty($inserted_ids)) {
			throw new \Exception('Insert models fail: '.Db::link()->getLastError());
		}
		$options = [];
		$options[static::incrColName()] = [$inserted_ids, 'IN'];
		$created = static::get($options);
		
		$created->each(function(&$model) {
			$model->_onCreate();
			$model->trigger('created');
		});
		return $created;
	}
	
    /**
     * Insert no increment models to DB
	 * @param array $models data
	 * @return Collection
     */
	public static function insertNoIncrement($models_data = [])
	{
		$emptyModel = new static([]);
		$aviableFields = array_merge($emptyModel->writableFields(), [static::incrColName()]);
		
		foreach ($models_data as &$data) {
			foreach ($data as $field_key=>$field_val) {
				if (!in_array($field_key, $aviableFields)) {
					unset($data[$field_key]);
				}
			}
			$data['created_at'] = dateFor('Y-m-d H:i:s');
			$data['modified_at'] = dateFor('Y-m-d H:i:s');
		}
		$results = Db::link()->insertMulti(static::getTableName(), $models_data);
		if (empty($results)) {
			throw new \Exception('Insert models fail: '.Db::link()->getLastError());
		}
		$inserted_ids = [];
		foreach ($results as $key=>$result) {
			$inserted_ids[]= $models_data[$key][static::incrColName()];
		}
		$options = [];
		$options[static::incrColName()] = [$inserted_ids, 'IN'];
		$created = static::get($options);
		
		$created->each(function(&$model) {
			$model->_onCreate();
			$model->trigger('created');
		});
		return $created;
	}
	
    /**
     * Insert or update no increment models to DB
	 * @param array $models data
	 * @return Collection|Model
     */
	public static function insertOrUpdate($model_data = [])
	{
		if (is_object($model_data)) {
			$model_data = (array)$model_data;
		}
		if (isset($model_data[0])) {
			throw new \Exception('Insert model data must be fields array');
		}
		$emptyModel = new static([]);
		$aviableFields = array_merge($emptyModel->writableFields(), [static::incrColName()]);
		
		foreach ($model_data as $field_key=>$field_val) {
			if (!in_array($field_key, $aviableFields)) {
				unset($model_data[$field_key]);
			}
		}
		$model_data['created_at'] = dateFor('Y-m-d H:i:s');
		$model_data['modified_at'] = dateFor('Y-m-d H:i:s');
		$updateFields = array_keys($model_data);
		
		foreach($updateFields as $upd_field_key=>$upd_field_val) {
			if (in_array($upd_field_val, ['created_at'])) {
				unset($updateFields[$upd_field_key]);
			}
		}
		Db::link()->onDuplicate($updateFields);
		if (!Db::link()->insert(static::getTableName(), $model_data)) {
			throw new \Exception('Insert model fail: '.Db::link()->getLastError());
		}
		return static::find($model_data[static::incrColName()]);
	}
	
    /**
     * Save model
	 * @return Model
     */
	public function save()
	{
		if (false === $this->trigger('saving')) {
			return false;
		}
		$writeAttributes = $this->_getWriteAttributes();
		$this->attributes['modified_at'] = dateFor('Y-m-d H:i:s');
		$writeAttributes['modified_at'] = $this->attributes['modified_at'];
		
		Db::link()->where(static::incrColName(), $this->attributes[static::incrColName()])
				  ->update(static::getTableName(), $writeAttributes);
		if (Db::link()->getLastErrno() === 0) {
			$this->changed = [];
			$this->_onSave();
			$this->trigger('saved');
			return $this;
		}
		throw new \Exception('Error save model: '.Db::link()->getLastError());
	}
	
    /**
     * Remove all models
	 * @return bool
     */
	public static function removeAll()
	{
		Db::link()->delete(static::getTableName());
		if (Db::link()->getLastErrno() === 0) {
			return true;
		}
		return false;
	}
	
    /**
     * Remove model
	 * @return bool
     */
	public function remove()
	{
		if (false === $this->trigger('removing')) {
			return false;
		}
		Db::link()->where(static::incrColName(), $this->attributes[static::incrColName()])
				  ->delete(static::getTableName());
		if (Db::link()->getLastErrno() === 0) {
			$this->trigger('removed');
			return true;
		}
		return false;
	}
	
    /**
     * On model create trigger
     */
	public function _onCreate()
	{
		// code...
	}
	
    /**
     * On model save trigger
     */
	public function _onSave()
	{
		// code...
	}
	
    /**
     * Model event trigger
	 * @param string $event
	 * @param array $args
     */
	public function trigger($event, $args = [])
	{
		$args['model'] = $this;
		return events()->trigger(static::eventsNamespace().'.'.$event, $args);
	}
	
    /**
     * On model event
	 * @param string $event
	 * @param mixed $handler
     */
	public static function on($event, $handler)
	{
		return events()->on(static::eventsNamespace().'.'.$event, $handler);
	}
}
