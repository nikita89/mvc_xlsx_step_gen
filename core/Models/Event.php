<?php
/**
 * Web Application Event
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace Core\Models;

class Event
{
	protected 
		$action,
		$caller,
		$arguments;
	
    /**
     * Constructor
	 * @param string $action
	 * @param array $event_args
	 * @param object $caller
     */
    public function __construct($action, Array $event_args, $caller = [])
    {
		foreach ($event_args as $key=>$val) {
			$this->arguments[$key] = $val;
		}
		$this->action = $action;
		$this->caller = $caller;
    }
	
	/**
     * Get called event action
	 * @return string
     */
	public function getAction()
	{
		return $this->action;
	}
	
	/**
     * Get event caller
	 * @return string
     */
	public function getCaller()
	{
		return $this->caller;
	}
	
	/**
     * Protect get event args
	 * @param string $key
     */
	public function __get($key)
	{
		if (array_key_exists($key, $this->arguments)) {
			return $this->arguments[$key];
		}
		throw new \Exception('Invalid action argument: '.$key);
	}
}
