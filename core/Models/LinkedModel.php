<?php
/**
 * Web Application Linked Model
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace Core\Models;
use Components\Database\Connection as Db;

class LinkedModel extends Model
{
	use Traits\DbTable, Traits\Detector, Traits\Converter;

	protected 
		$system = [
			'link_id' 
		],
		$table_name;
	
    /**
     * Constructor
	 * @param mixed $data
	 * @param array $field_list
	 * @param string $table_name
     */
    public function __construct($data, $field_list, $table_name)
    {
		if (!is_array($data) && !is_object($data)) {
			throw new \Exception(static::getBasename().' model data must be array or object');
		}
        foreach ($field_list as $field_key) {
			$this->writable[]= $field_key;
		}
        foreach ($this->system as $field_key) {
			$this->attributes[$field_key] = null;
		}
        foreach ($this->writable as $field_key) {
			$this->attributes[$field_key] = null;
		}
        foreach ($data as $field_key=>$val) {
			if (array_key_exists($field_key, $this->attributes)) {
				$this->attributes[$field_key] = $val;
			}
		}
		$this->table_name = $table_name;
    }
	
    /**
     * Save model
	 * @return Model
     */
	public function save()
	{
		Db::link()->where('link_id', $this->attributes['link_id'])
				  ->update($this->table_name, $this->_getWriteAttributes());
		if (Db::link()->getLastErrno() === 0) {
			return $this;
		}
		throw new \Exception('Error save model: '.Db::link()->getLastError());
	}
}
