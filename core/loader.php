<?php
/**
 * Web Application initialize
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
if (!defined('ROOT')) {
    exit('Forbidden');
}
define('BASE_ROOT', ROOT);
define('APP', ROOT . '/app');
define('CONFIG', ROOT . '/config');
define('STORAGE', APP . '/Storage');
define('CLASSES', ROOT . '/classes');
define('RESOURCES', ROOT . '/resources');
define('LOGS', RESOURCES . '/logs');

error_reporting(E_ALL);
ignore_user_abort(1);

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
//ini_set('max_execution_time', 300);
ini_set('ignore_user_abort', 1);
ini_set('log_errors', 1);
ini_set('error_log', LOGS . '/php/error.log');

ini_set('upload_tmp_dir', '/home/admin/tmp');
putenv('TMPDIR=/home/admin/tmp');

require ROOT . '/vendor/autoload.php';
require ROOT . '/core/System/Functions.php';

set_error_handler('error_handler', E_ALL);	
set_exception_handler('exception_handler');
spl_autoload_register('autoload_handler');

date_default_timezone_set(config('timezone'));
setlocale(LC_ALL, config('locale'));

define('APP_URI', config('appuri'));
define('HOST_URL', config('hosturl'));
define('APP_URL', HOST_URL . APP_URI);
define('ASSETS_URL', APP_URL . '/assets');
define('RESOURCES_URL', APP_URL . '/resources');

return \Core\Application::getInstance();
