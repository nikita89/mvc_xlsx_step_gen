<?php
/**
 * Google Drive config file
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
$config = [
	'base_url' => 'https://servicepanel.ru/',
	'create_folder' => [
		'route' => 'google_disk_api/createFolder',
		'parent_id' => '17627ownfj4kUIM_MVAGirTv4m-aiLKJ-', // parent directory
		'widget_id' => 'ca5f9a505519deafdd6f81466e8d6b80',
		'folder_name' => '', // name of created folder
		'token' => 'e25a15b39f07a94dff5ce95aee5d7e12'
	],
	'upload_file' => [
		'route' => 'google_disk_api/uploadFromFolder',
		'folder_id' => '', // directory where file is put
		'widget_id' => 'ca5f9a505519deafdd6f81466e8d6b80',
		'file_name' => '', // name of uploaded file
		'token' => 'e25a15b39f07a94dff5ce95aee5d7e12',
		'file_url' => '' // url of uploaded file
	],
];
