<?php
/**
 * Web Application Crypto config file
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
$config = [
	'wrapper' => '\Components\Security\Wrappers\OpenSSL',
];
