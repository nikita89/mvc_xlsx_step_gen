<?php
/**
 * Web Application DB config file
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
$config = [
    'host' => 'localhost',
    'username' => 'root',
    'password' => '',
    'db' => 'googlesync',
    'port' => 3306,
    'prefix' => '',
    'charset' => 'utf8'
];
