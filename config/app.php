<?php
/**
 * Web Application config file
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
$config = [
	// Предполагаемый заголовок сайта
	'apptitle' => 'MVC Platform',
	
	// Локаль в PHP (по умолч. ru_RU.UTF-8)
	'locale' => 'ru_RU.UTF-8',
	// Часовой пояс (по умолч. МСК)
	'timezone' => 'Europe/Moscow',
	// Часовой пояс (по умолч. МСК)
	'hours_offset' => 0, # 1,-2
	
	'appuri' => '', # /example for dir
	'hosturl' => 'https://s120035.savps.ru',
	'real_path_offset' => '', # /example for dir

	// буферизация вывода в PHP (по умолч. вкл)
	'output_buffer' => 1,
	// Минификация HTML кода, отдаваемого вьюхами (по умолч. выкл)
	'output_minify' => 0,
	
	// Уникальный ключ приложения (22 символа)
	'key' => 'kt!6&u7dr$g236uj1@349f',
];
