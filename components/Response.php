<?php
/**
 * Web Application Response class
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace Components;
use Components\Support\Str,
	App\Services\Minify;

abstract class Response
{
	private static 
		$_buffer = false,
		$_content = [
			'mime' => 'text/html',
			'charset' => 'utf-8',
			'body' => '',
		],
		$_minify_mimes = [
			'text/html', 'text/xml', 'application/xml'
		];
		 
    /**
     * Enable out buffer
     */
    public static function obStart()
    {
        self::$_buffer = ob_start();
    }
	
    /**
     * Allow origin header
	 * @param string $value allow to
     */
    public static function allowOrigin($value = '*')
    {
        header('Access-Control-Allow-Origin: ' . $value);
    }

    /**
     * Set Content type header
	 * @param string $mime content mime type
	 * @param string $charset content charset
     */
    public static function contentType($mime = 'text/html', $charset = 'utf-8')
    {
		self::$_content['mime'] = $mime;
		self::$_content['charset'] = $charset;
    }

    /**
     * Location to
	 * @param string $destination redirect URI
     */
    public static function location($destination = '/')
    {
        header('Location: ' . $destination);
        self::html('<script>window.location="' . $destination . '";</script>');
    }
	
    /**
     * Out data
	 * @param bool $minify flag
     */
    public static function out($data = null)
    {
		if (self::$_buffer) {
			self::_cleanBuffer();
			header('Content-Type: '.self::$_content['mime'].'; charset='.self::$_content['charset']);
		}
		if (!is_null($data)) {
			self::$_content['body'] = Str::toString($data);
		}
		if (config('output_minify') == 1 && in_array(self::$_content['mime'], self::$_minify_mimes)) {
			self::$_content['body'] = Minify::html(self::$_content['body']);
		}
		echo self::$_content['body'];
		exit;
    }
	
	/**
	 * Out application/json
	 * @param mixed $data json data
	 */
	public static function json($data)
	{
		self::contentType('application/json');
		self::out(jsonEncode($data));
	}
	
	/**
	 * Out plain/text
	 * @param string $data text data
	 */
	public static function text($data)
	{
		self::contentType('text/plain');
		self::out($data);
	}
	
	/**
	 * Out text/html
	 * @param string $data html data
	 */
	public static function html($data)
	{
		self::contentType('text/html');
		self::out($data);
	}
	
    /**
     * Get out buffer and clean
     */
    private static function _cleanBuffer()
    {
		self::$_content['body'] = trim(Str::toString(ob_get_clean()));
	}
	
	/**
	 * Download file
	 * @param string $file_path
	 * @param string $mime_type
	 * @param mixed $file_name
	 */
	public static function download($file_path, $file_name = null, $mime_type = 'application/octet-stream')
	{
		ob_end_clean();
		if (is_null($file_name)) {
			$fnames = explode('/', $file_path);
			if (count($fnames) > 1) {
				$file_name = end($fnames);
			} else {
				$file_name = 'file'.getTime();
			}
		}
        header('Content-Type: '.$mime_type);
		header('Content-Disposition: attachment; filename="'.$file_name.'";');
		header('Content-Transfer-Encoding: binary');
		header('Content-Length: '.filesize($file_path));
        readfile($file_path);
	}
}
