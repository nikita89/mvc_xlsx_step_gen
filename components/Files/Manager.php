<?php
/**
 * Web Application File Manager class
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace Components\Files;

class Manager
{
	private 
		$_path;
	
    /**
     * Constructor
	 * @param mixed $path
     */
	public function __construct($path = null)
    {
		if (is_null($path)) {
			$path = '/resources/';
		}
		$this->setDirectory($path);
	}
	
    /**
     * Get current path
	 * @return string
     */
    public function getPath()
    {
		return $this->_path;
	}
	
    /**
     * Start session
	 * @param string $path
     */
    public function setDirectory($path)
    {
		if (!is_string($path)) {
			throw new \Exception('The path must be string');
		}
		if ($path == '') {
			throw new \Exception('The path must be not empty');
		}
		if (mb_strcut($path, -1) !== '/') {
			throw new \Exception('The path must be ending in "/"');
		}
		$this->_path = ROOT . $path;
    }
	
	/**
	 * If exists file
	 * @param string $file
	 * @return mixed
	 */
	public function exists($file = null)
	{
		if (is_null($file)) {
			return file_exists($this->_path);
		}
		if ($file == '') {
			throw new \Exception('Filename must be not empty');
		}
		if (file_exists($this->_path . $file)) {
			return true;
		}
		return false;
	}
	
	/**
	 * Create file
	 * @param string $file
	 * @param mixed $data
	 * @param integer $cmod
	 * @return mixed
	 */
	public function createFile($file, $data = null, $cmod = 0666)
	{
		if ($this->exists($file)) {
			throw new \Exception('File exists: '.$file);
		}
		if ($this->put($file, $data)) {
			@chmod($this->_path . $file, $cmod);
		}
		return $this->exists($file);
	}
	
	/**
	 * Create file
	 * @param string $path
	 * @param integer $cmod
	 * @return mixed
	 */
	public function createDir($path = '', $cmod = 0666)
	{
		if ($result = @mkdir($this->_path . $path, 0777, true)) {
			@chmod($this->_path . $path, 0777);
			return true;
		}
		return false;
	}
	
	/**
	 * Get filedata
	 * @param string $file
	 * @param integer $flags
	 * @return mixed
	 */
	public function file($file, $flags = FILE_IGNORE_NEW_LINES)
	{
		if (!$this->exists($file)) {
			return false;
		}
		$data = file($this->_path . $file, $flags);
		if (!is_array($data)) {
			return false;
		}
		return collection($data);
	}
	
	/**
	 * Get filedata
	 * @param string $file
	 * @param mixed $default
	 * @return mixed
	 */
	public function get($file, $default = null)
	{
		if (!$this->exists($file)) {
			return $default;
		}
		return file_get_contents($this->_path . $file);
	}
	
	/**
	 * Set filedata
	 * @param string $file
	 * @param mixed $data
	 * @return bool
	 */
	public function put($file, $data = null)
	{
		if ($file == '') {
			throw new \Exception('Filename must be not empty');
		}
		if (file_put_contents($this->_path . $file, $data)) {
			return true;
		}
		return false;
	}
	
	/**
	 * Append filedata
	 * @param string $file
	 * @param mixed $data
	 * @return bool
	 */
	public function append($file, $data = null)
	{
		if ($file == '') {
			throw new \Exception('Filename must be not empty');
		}
		if (file_put_contents($this->_path . $file, $data, FILE_APPEND | LOCK_EX)) {
			return true;
		}
		return false;
	}
	
	/**
	 * Get file modified time
	 * @param string $file
	 * @param mixed $default
	 * @return mixed
	 */
	public function getModified($file, $default = false)
	{
		if (!$this->exists($file)) {
			return $default;
		}
		return filemtime($this->_path . $file);
	}
	
	/**
	 * Remove file
	 * @param string $file
	 * @return bool
	 */
	public function remove($file)
	{
		if (!$this->exists($file)) {
			return false;
		}
		return unlink($this->_path . $file);
	}
	
	/**
	 * Remove dir
	 * @param string $path
	 * @param bool $recursive
	 * @return bool
	 */
	public function removeDir($path = '', $recursive = true)
	{
        $files = array_diff(scandir($this->_path.$path), ['.', '..']); 
        foreach ($files as $file) {
            is_dir($this->_path.$path.$file) ? $this->removeDir($path.$file.'/') : unlink($this->_path.$path.$file); 
        }
		return rmdir($this->_path.$path);
	}
	
	/**
	 * Search files
	 * @param string $pattern
	 * @return bool
	 */
	public function search($pattern = '*')
	{
		$searched = [];
		$files = glob($this->_path .'/'.$pattern);
		foreach($files as $file) {
			$searched[]= str_replace($this->_path.'/', '', $file);
		}
		return collection($searched);
	}
}
