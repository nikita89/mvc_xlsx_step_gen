<?php
/**
 * Web Application DB buffer
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace Components\Database;

abstract class Buffer
{
    private static $_contents = [];

    /**
     * Has bufer value 
	 * @param string $query - sql query hash
	 * @return bool
     */
    public static function has($query)
    {
		return array_key_exists($query, self::$_contents);
    }
	
    /**
     * Set bufer value 
	 * @param string $query - sql query hash
	 * @param mixed $content - sql result
	 * @return mixed
     */
    public static function set($query, $content)
    {
        self::$_contents[$query] = $content;
		return self::$_contents[$query];
    }

    /**
     * Get buffer value
	 * @param string $query - sql query hash
	 * @return mixed
     */
    public static function get($query)
    {	
		if (!array_key_exists($query, self::$_contents)) {
			return null;
		}
        return self::$_contents[$query];
    }
	
    /**
     * Remove buffer value
	 * @param string $query - sql query hash
	 * @return bool
     */
    public static function remove($query)
    {
		if (array_key_exists($query, self::$_contents)) {
			unset(self::$_contents[$query]);
			return true;
		}
		return false;
    }
}
