<?php
/**
 * Web Application DB connection class
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace Components\Database;

abstract class Connection
{
    private static $_db = [];

    /**
     * Get DB link
	 * @param string $link
	 * @return object
     */
    public static function link($link = 'default')
    {
        if (!isset(self::$_db[$link])) {
            self::connect($link);
        }
        return self::$_db[$link];
    }

    /**
     * DB connect
	 * @param string $link
	 * @return void
     */
    private static function connect($link)
    {
        self::$_db[$link] = new Mysqli\MysqliDb(config('db.'));
    }
}
