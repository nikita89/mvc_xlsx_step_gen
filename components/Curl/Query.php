<?php
/**
 * Curl query class
 */
namespace Components\Curl;

class Query
{
    protected
		$curl,
		$url,
		$options = [
			'user_agent' => 'WebApplication HTTP Client/1117',
			'use_cookies' => false,
			'post_data_format' => 'raw', // raw|json
			'autoreferer' => true,
			'ssl_verifyhost' => false,
			'ssl_verifypeer' => false,
			'returntransfer' => true,
			'followlocation' => true,
			'headers' => [], // key=>val
		],
		$cookie_key;
	
	/**
	 * Constructor
	 * @param string $url target URL
	 * @param array $options
	 */
    public function __construct($url, $options = [])
    {
		$this->url = $url;
		$this->options = array_merge($this->options, $options);
        $this->curl = \curl_init();
		
		curl_setopt($this->curl, CURLOPT_USERAGENT, $this->options['user_agent']);
		curl_setopt($this->curl, CURLOPT_AUTOREFERER, $this->options['autoreferer']);
		curl_setopt($this->curl, CURLOPT_SSL_VERIFYHOST, $this->options['ssl_verifyhost']);
		curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, $this->options['ssl_verifypeer']);
		curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, $this->options['returntransfer']);
		curl_setopt($this->curl, CURLOPT_FOLLOWLOCATION, $this->options['followlocation']);
		
		if (!empty($this->options['headers'])) {
			$this->setHeaders($this->options['headers']);
		}
		if ($this->options['use_cookies']) {
			$url = mb_parse_url($url);
			$this->cookie_key = md5($url['scheme'].$url['host']);
			curl_setopt($this->curl, CURLOPT_COOKIEJAR, STORAGE.'/Curl/cookie_'.$this->cookie_key.'.txt');
			curl_setopt($this->curl, CURLOPT_COOKIEFILE, STORAGE.'/Curl/cookie_'.$this->cookie_key.'.txt');
		}
    }

    /**
     * Set query headers
	 * @param array $headers
     */
    public function setHeaders($headers = [])
    {
		$this->options['headers'] = array_merge($this->options['headers'], $headers);
		curl_setopt($this->curl, CURLOPT_HTTPHEADER, $this->getHeaders());
	}
	
    /**
     * Get query headers
	 * @return array
     */
    public function getHeaders()
    {
		$headers = [];
		foreach ($this->options['headers'] as $key=>$val) {
			$headers[]= $key.': '.$val;
		}
		return $headers;
	}

    /**
     * GET query
	 * @param array $args
     */
    public function get($args = [])
    {
        curl_setopt($this->curl, CURLOPT_URL, $this->buildUrl($args));
        return $this->response();
    }

    /**
     * POST query
	 * @param array $data
	 * @param array $args
     */
    public function post($data = [], $args = [])
    {
        curl_setopt($this->curl, CURLOPT_URL, $this->buildUrl($args));
        curl_setopt($this->curl, CURLOPT_POST, true);
		if ($this->options['post_data_format'] == 'json') {
			$post_data = json_encode($data);
			curl_setopt($this->curl, CURLOPT_POSTFIELDS, $post_data);
			$this->setHeaders(['Content-Type' => 'application/json']);
		} else {
			curl_setopt($this->curl, CURLOPT_POSTFIELDS, http_build_query($data));
		}
        return $this->response();
    }
	
    /**
     * PUT query
	 * @param array $data
	 * @param array $args
     */
    public function put($data = [], $args = [])
    {
        curl_setopt($this->curl, CURLOPT_URL, $this->buildUrl($args));
        curl_setopt($this->curl, CURLOPT_CUSTOMREQUEST, 'PUT');
		if ($this->options['post_data_format'] == 'json') {
			$post_data = json_encode($data);
			curl_setopt($this->curl, CURLOPT_POSTFIELDS, $post_data);
			$this->setHeaders(['Content-Type' => 'application/json']);
		} else {
			curl_setopt($this->curl, CURLOPT_POSTFIELDS, http_build_query($data));
		}
        return $this->response();
    }

    /**
     * Create link
	 * @param array $args
     */
    private function buildUrl($args = [])
    {
        if (!empty($args)) {
            $this->url .= '?' . http_build_query($args);
        }
        return $this->url;
    }

    /**
     * Response
     */
    private function response()
    {
		$data = curl_exec($this->curl);
        return new Response($data, $this->curl);
    }

    /**
     * Close curl
     */
    public function __destruct()
    {
        if (!is_null($this->curl)) {
			curl_close($this->curl);
		}
    }
}
