<?php
/**
 * Auth class
 */
namespace Components;
use Core\Traits,
	App\Models\User;

class Auth
{
	use Traits\Singleton;
	
	protected 
		$_user;
	
    /**
     * Check current user auth
	 * @return bool
     */
    public function check()
    {
		$cookie = (array)session()->getCookie();
		$session_user_id = session()->get('user_id');

		if (!isset($cookie['user_id']) || !is_numeric($session_user_id)) {
			return false;
		}
		if ($cookie['user_id'] !== $session_user_id) {
			return false;
		}
		return true;
	}
	
    /**
     * Attempt user login
	 * @param array $where
	 * @param bool $remember
	 * @return bool
     */
    public function attempt($where = ['login' => '', 'password' => ''], $remember = false)
    {
		$this->trigger('attempt', [
			'where' => $where,
			'remember' => $remember
		]);
		if (!$user = $this->validate($where)) {
			return false;
		}
		return $this->login($user, $remember);
	}
	
    /**
     * Validate current user creds
	 * @param array $where
	 * @return bool
     */
    public function validate($where = ['login' => '', 'password' => ''])
    {
		$password = $where['password'];
		unset($where['password']);
		
		if (!$user = User::get($where)->first()) {

			return false;
		}
		if (!$user->hasPassword($password)) {
			return false;
		}
		return $user;
	}
	
    /**
     * Authenticate user
	 * @param User $user 
	 * @param bool $remember
	 * @return bool
     */
    public function login($user, $remember = false)
    {
		if (!config('session.enable')) {
			throw new \Exception('Login failed: session disabled');
		}
		$this->_user = $user;
		
		session()->updateCookie([
			'user_id' => $user->id
		]);
		session()->set('user_id', $user->id);
		if (!$this->check()) {
			return false;
		}
		if ($remember) {
			$this->remember(true);
		}
		$this->trigger('login', [
			'user' => $user,
			'remember' => $remember
		]);
		return true;
	}
	
    /**
     * Remember user
	 * @param bool $status 
	 * @return bool
     */
    public function remember($status)
    {
		return session()->setLongStatus($status);
	}
	
    /**
     * Get logged user id
	 * @return User
     */
    public function id()
    {
		if (!$this->check()) {
			throw new \Exception('User is not authentificated');
		}
		return session()->get('user_id');
	}
	
    /**
     * Get logged user model
	 * @return User
     */
    public function user()
    {
		if (is_null($this->_user)) {
			$this->_user = User::find($this->id());
		}
		return $this->_user;
	}
	
    /**
     * Clear auth session
     */
    public function clear()
    {
		session()->clear();
	}
	
    /**
     * Logout current user 
     */
    public function logout()
    {
		session()->destroy();
		$this->trigger('logout');
		$this->_user = null;
	}
	
    /**
     * Auth event trigger
	 * @param string $event
	 * @param array $args
     */
	public function trigger($event, $args = [])
	{
		if ($this->check()) {
			$args['user'] = $this->user();
		}
		return events()->trigger('auth.'.$event, $args);
	}
}