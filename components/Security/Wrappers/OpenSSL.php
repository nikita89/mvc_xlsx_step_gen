<?php
/**
 * Web Application Crypto wrapper class
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace Components\Security\Wrappers;

class OpenSSL
{
	private $key,
			$iv;
    /**
     * Constructor
     */
    public function __construct()
    {
		$this->key = md5(config('key'));
		$this->iv = substr($this->key, 8, 16);
	}
	
    /**
     * Encrypt data
	 * @param string $data
	 * @return string
     */
    public function encrypt($data)
    {
		return openssl_encrypt($data, 'AES-256-CBC', $this->key, 0, $this->iv);
    }
	
   /**
     * Decrypt data
	 * @param string $data
     * @return string
     */
    public function decrypt($data)
    {
        return openssl_decrypt($data, 'AES-256-CBC', $this->key, 0, $this->iv);
	}
}