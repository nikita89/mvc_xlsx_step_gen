<?php
/**
 * Web Application Crypto wrapper class
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace Components\Security\Wrappers;

class Base64
{
	private $key;
	
    /**
     * Constructor
     */
    public function __construct()
    {
		$this->key = md5(config('key'));
	}
	
    /**
     * Encrypt data
	 * @param string $data
	 * @return string
     */
    public function encrypt($data)
    {
		return base64_encode($data);
    }
	
   /**
     * Decrypt data
	 * @param string $data
     * @return string
     */
    public function decrypt($data)
    {
        return base64_decode($data);
	}
}