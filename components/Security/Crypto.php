<?php
/**
 * Web Application Crypto class
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace Components\Security;

abstract class Crypto
{
	private static
		$_wrapper;
	
    /**
     * Encrypt data
	 * @param string $data
	 * @return string
     */
    public static function encrypt($data)
    {
		return static::$_wrapper->encrypt(
			base64_encode(serialize($data))
		);
    }
	
   /**
     * Decrypt data
	 * @param string $data
     * @return string
     */
    public static function decrypt($data)
    {
        return unserialize(base64_decode(
			static::$_wrapper->decrypt($data)
		));
	}
	
   /**
     * Init Crypto wrapper
     * @return string
     */
    public static function init()
    {
        if (is_null(static::$_wrapper)) {
			$wrapper = config('crypto.wrapper');
			if (!class_exists($wrapper)) {
				throw new \Exception('Crypto wrapper is not defined: '.$wrapper);
			}
			static::$_wrapper = new $wrapper;
		}
	}
}