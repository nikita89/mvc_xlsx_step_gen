<?php
/**
 * Web Application Request class
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace Components;
use Core\Traits;

class Request
{
	use Traits\Singleton;
	
	private 
		$server,
		$ip;
		
    /**
     * Constructor
     */
	private function __construct()
    {
		$this->server = $_SERVER;
		$this->ip = $this->_getIP();
		
		if ($this->hasCli()) {
			$args = $this->server['argv'];
			unset($args[0]);
			foreach($args as $key=>$val) {
				$_GET[$key]= $val;
				$_REQUEST[$key]= $val;
			}
		}
	}

    /**
     * Get headers
	 * @return array
     */
    public function headers()
    {
		if (function_exists('getallheaders')) {
			return getallheaders();
		}
       $headers = []; 
       foreach ($_SERVER as $name=>$value) { 
           if (substr($name, 0, 5) == 'HTTP_') { 
               $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value; 
           } 
       } 
       return $headers; 
    }
	
    /**
     * Get from GET
	 * @param string $key request data key
	 * @param mixed $default default return data
	 * @return mixed
     */
    public function get($key = null, $default = null)
    {
        if (is_null($key)) {
            return $_GET;
        }
        if (!isset($_GET[$key])) {
            return $default;
        }
        return $_GET[$key];
    }

    /**
     * Get from POST
	 * @param string $key request data key
	 * @param mixed $default default return data
	 * @return mixed
     */
    public function post($key = null, $default = null)
    {
        if (is_null($key)) {
            return $_POST;
        }
        if (!isset($_POST[$key])) {
            return $default;
        }
        return $_POST[$key];
    }

    /**
     * Get from REQUEST
	 * @param string $key request data key
	 * @param mixed $default default return data
	 * @return mixed
     */
    public function input($key = null, $default = null)
    {
        if (is_null($key)) {
            return $_REQUEST;
        }
        if (!isset($_REQUEST[$key])) {
            return $default;
        }
        return $_REQUEST[$key];
    }

    /**
     * Get from input
	 * @return mixed
     */
    public function raw()
    {
		return file_get_contents('php://input');
	}

    /**
     * Get JSON from REQUEST
	 * @param string $key request data key
	 * @return object|array|null
     */
    public function json($key = null)
    {
		if (is_null($key)) {
			return json_decode($this->raw());
		}
        if (empty($_REQUEST[$key]) || !$json = json_decode($_REQUEST[$key])) {
            return null;
        }
        return $json;
    }

    /**
     * Get Cookie
	 * @param string $name cookie data key
	 * @param mixed $default default return data
	 * @return string
     */
    public function cookie($name, $default = null)
    {
        if (!isset($_COOKIE[$name])) {
			return $default;
        }
        return $_COOKIE[$name];
    }

    /**
     * Get HTTP Referer
	 * @return string|null
     */
    public function referer()
    {
        if (empty($this->server['HTTP_REFERER'])) {
			return null;
        }
        return $this->server['HTTP_REFERER'];
    }

    /**
     * Get HTTP Origin
	 * @return string|null
     */
    public function origin()
    {
        if (empty($this->server['HTTP_ORIGIN'])) {
			return null;
        }
        return $this->server['HTTP_ORIGIN'];
    }
	
    /**
     * Get client IP address
	 * @return string|null
     */
    public function acceptLang()
    {
        if (empty($this->server['HTTP_ACCEPT_LANGUAGE'])) {
			return null;
        }
        return $this->server['HTTP_ACCEPT_LANGUAGE'];
    }

    /**
     * Get client IP address
	 * @return string|null
     */
    public function userAgent()
    {
        if (empty($this->server['HTTP_USER_AGENT'])) {
			return null;
        }
        return $this->server['HTTP_USER_AGENT'];
    }
	
    /**
     * Get client IP address
	 * @return string
     */
    public function clientIP()
    {
		return $this->ip;
	}

    /**
     * Get request URI
	 * @return string
     */
    public function uri()
    {
		if ($this->hasCli() && !empty($this->server['argv'][1])) {
			return $this->server['argv'][1];
		}
        return $this->server['REQUEST_URI'];
    }
	
    /**
     * Has console request
	 * @return bool
     */
    public function hasCli()
    {
		return !isset($this->server['HTTP_HOST'], $this->server['REQUEST_URI']) && isset($this->server['argv']);
	}
	
    /**
     * Get request method
	 * @return string
     */
    public function method()
    {
		if ($this->hasCli()) {
			return 'GET';
		}
        return $this->server['REQUEST_METHOD'];
    }
	
    /**
     * Get client IP address
	 * @return string
     */
    private function _getIP()
    {
		if (!empty($this->server['HTTP_CLIENT_IP'])) {
			return $this->server['HTTP_CLIENT_IP'];
		}
		if (!empty($this->server['HTTP_X_FORWARDED_FOR'])) {
			return $this->server['HTTP_X_FORWARDED_FOR'];
		}
		if (!empty($this->server['HTTP_X_FORWARDED'])) {
			return $this->server['HTTP_X_FORWARDED'];
		}
		if (!empty($this->server['HTTP_FORWARDED_FOR'])) {
			return $this->server['HTTP_FORWARDED_FOR'];
		}
		if (!empty($this->server['HTTP_FORWARDED'])) {
			return $this->server['HTTP_FORWARDED'];
		}
		if (!empty($this->server['REMOTE_ADDR'])) {
			return $this->server['REMOTE_ADDR'];
		}
		return '127.0.0.1';
    }
}
