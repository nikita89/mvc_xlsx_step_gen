<?php
/**
 * Web Application Validator class
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace Components;
use Components\Support\Str;
 
class Validator {
	
	private $_input_data = [],
			$_filtered_data = [],
			$_models = [],
			$_errors = [];
			
	/**
	 * Construct validator
	 * @param array $input validate input
	 * @param rules $rules validate rules
	 */
	public function __construct($input = [], $rules = [])
	{
		$this->_input_data = $input;
		foreach ($rules as $field_name=>$rule) {
			
			$this->_filtered_data[$field_name] = null;
			$rule = explode('|', $rule);
			
			foreach ($rule as $rule_item) {
				
				$rule_method = $rule_item;
				$rule_args = null;
				
				if (strpos($rule_item, ':')) {
					$rule_item = explode(':', $rule_item);
					$rule_method = $rule_item[0];
					$rule_args = $rule_item[1];
				}
				$processor = 'processRule_'.$rule_method;
				if (!method_exists($this, $processor)) {
					throw new \Exception('Unregistered validator rule: '.$rule_method);
				}
				$this->_filtered_data[$field_name] = $this->$processor( $field_name, $rule_args );
			}
		}
	}
	
    /**
	 * Check field isset
	 * @param string $key validate field key
	 * @return mixed
     */
	private function checkField($key)
	{
		if (!isset($this->_input_data[$key])) {
			$this->_input_data[$key] = null;
		}
		return $this->_input_data[$key];
	}
	
    /**
	 * Validate rule - optional
	 * @param string $key validate field key
	 * @return mixed
     */
	private function processRule_optional($key)
	{
		return $this->checkField($key);
	}
	
    /**
	 * Validate rule - required
	 * @param string $key validate field key
	 * @return mixed
     */
	private function processRule_required($key)
	{
		if (!isset($this->_input_data[$key])) {
			$this->error($key, 'Не заполнено поле');
			$this->_input_data[$key] = null;
		}
		return $this->_input_data[$key];
	}
	
    /**
	 * Validate rule - string
	 * @param string $key validate field key
	 * @return string
     */
	private function processRule_string($key)
	{
		$this->checkField($key);
		if (!is_string($this->_input_data[$key])) {
			$this->error($key, 'Поле не является строкой');
		}
		return Str::toString($this->_input_data[$key]);
	}
	
    /**
	 * Validate rule - numeric
	 * @param string $key validate field key
	 * @return string
     */
	private function processRule_numeric($key)
	{
		$this->checkField($key);
		if (!is_numeric($this->_input_data[$key])) {
			$this->error($key, 'Поле не является числом');
		}
		return Str::toNumeric($this->_input_data[$key]);
	}
	
    /**
	 * Validate rule - array
	 * @param string $key validate field key
	 * @return string
     */
	private function processRule_array($key)
	{
		$this->checkField($key);
		if (!is_array($this->_input_data[$key])) {
			$this->error($key, 'Поле не является множеством');
		}
		return $this->_input_data[$key];
	}
	
    /**
	 * Validate rule - date
	 * @param string $key validate field key
	 * @return string
     */
	private function processRule_date($key)
	{
		$value = $this->processRule_string($key);
		if (!strtotime($value)) {
			$this->error($key, 'Поле не является датой');
		}
		return $value;
	}
	
    /**
	 * Validate rule - time
	 * @param string $key validate field key
	 * @return string
     */
	private function processRule_time($key)
	{
		$this->processRule_numeric($key);
		if ($value < 1) {
			$this->error($key, 'Поле не является меткой времени');
		}
		return $value;
	}
	
    /**
	 * Validate rule - phone
	 * @param string $key validate field key
	 * @return mixed
     */
	private function processRule_phone($key)
	{
		$this->checkField($key);
		$value = preg_replace('#[^+0-9\-\(\)]+#Uis', '', Str::toString($this->_input_data[$key]));
		if (strlen($value) < 5 || strlen($value) > 16) {
			$this->error($key, 'Не верно указан номер телефона');
		}
		return $value;
	}
	
    /**
	 * Validate rule - email
	 * @param string $key validate field key
	 * @return mixed
     */
	private function processRule_email($key)
	{
		$value = $this->processRule_string($key);
		if (!preg_match('#[^@]+@.+\.[a-zA-Z]+#Uis', $value) || strlen($value) < 7 || strlen($value) > 50) {
			$this->error($key, 'Не верно указан email');
		}
		return $value;
	}
	
    /**
	 * Validate rule - min length
	 * @param string $key validate field key
	 * @param string $arg validate length
	 * @return mixed
     */
	private function processRule_minl($key, $arg)
	{
		$value = Str::toString($this->_input_data[$key]);
		if (strlen($value) < $arg) {
			$this->error($key, 'Поле должно быть не меньше '.$arg.' символов');
		}
		return $value;
	}
	
    /**
	 * Validate rule - max length
	 * @param string $key validate field key
	 * @param string $arg validate length
	 * @return mixed
     */
	private function processRule_maxl($key, $arg)
	{
		$value = Str::toString($this->_input_data[$key]);
		if (strlen($value) > $arg) {
			$this->error($key, 'Поле должно быть не больше '.$arg.' символов');
		}
		return $value;
	}
	
    /**
	 * Validate rule - min count
	 * @param string $key validate field key
	 * @param string $arg validate count
	 * @return mixed
     */
	private function processRule_minc($key, $arg)
	{
		if (count($this->_input_data[$key]) < $arg) {
			$this->error($key, 'Количество элементов должно быть не меньше чем '.$arg);
		}
		return $this->_input_data[$key];
	}
	
    /**
	 * Validate rule - min count
	 * @param string $key validate field key
	 * @param string $arg validate count
	 * @return mixed
     */
	private function processRule_maxc($key, $arg)
	{
		if (count($this->_input_data[$key]) > $arg) {
			$this->error($key, 'Количество элементов должно быть не больше чем '.$arg);
		}
		return $this->_input_data[$key];
	}
	
    /**
	 * Validate rule - min val
	 * @param string $key validate field key
	 * @param string $arg validate length
	 * @return mixed
     */
	private function processRule_min($key, $arg)
	{
		$value = Str::toNumeric($this->_input_data[$key]);
		if ($value < $arg) {
			$this->error($key, 'Поле должно не меньше чем '.$arg);
		}
		return $value;
	}
	
    /**
	 * Validate rule - max val
	 * @param string $key validate field key
	 * @param string $arg validate length
	 * @return mixed
     */
	private function processRule_max($key, $arg)
	{
		$value = Str::toNumeric($this->_input_data[$key]);
		if ($value > $arg) {
			$this->error($key, 'Поле должно не больше чем '.$arg);
		}
		return $value;
	}
	
    /**
	 * Validate rule - max val
	 * @param string $key validate field key
	 * @param string $enums validate values
	 * @return mixed
     */
	private function processRule_enum($key, $enums)
	{
		$value = Str::toString($this->_input_data[$key]);
		$enum_values = explode(',', $enums);
		if (!in_array($value, $enum_values)) {
			$this->error($key, 'Поле должно иметь одно из значений: '.$enums);
		}
		return $value;
	}
	
    /**
	 * Validate rule - val is Model
	 * @param string $key validate field key
	 * @param string $arg validate model
	 * @return mixed
     */
	private function processRule_models($key, $arg)
	{
		$value = $this->processRule_required($key, $arg);
		$class = model_class($arg);
		$models = $class::get([
			$key => $value
		]);
		if ($models->count() == 0) {
			$this->error($key, 'Модель не найдена');
		}
		if (!isset($this->_models[$arg])) {
			$this->_models[$arg] = [];
		}
		$this->_models[$arg][$key] = $models;
		return $value;
	}
	
    /**
	 * Register validate error
	 * @param string $name field key
	 * @param string $msg error msg
     */
	public function error($key, $msg)
	{
		if (!isset($this->_errors[$key])) {
			$this->_errors[$key] = (object)array(
				'value' => $this->_filtered_data[$key],
				'messages' => array(),
			);
		}
		if (!in_array($msg, $this->_errors[$key]->messages)) {
			$this->_errors[$key]->messages[]= $msg;
		}
	}
	
    /**
	 * Get errors
	 * @return array
     */
	public function isValid()
	{
		return empty($this->_errors) ? true : false;
	}
	
    /**
	 * Get filtered data
	 * @return array
     */
	public function data()
	{
		return (object)$this->_filtered_data;
	}
	
    /**
	 * Get validated models
	 * @param string $name
	 * @param string $field
	 * @return array
     */
	public function models($name, $field = null)
	{
		if (!array_key_exists($name, $this->_models)) {
			return null;
		}
		if (!is_null($field)) {
			if (!array_key_exists($field, $this->_models[$name])) {
				return null;
			}
			return $this->_models[$name][$field];
		}
		return (object)$this->_models[$name];
	}
	
    /**
	 * Get error messages
	 * @return array
     */
	public function errors()
	{
		return $this->_errors;
	}
}
