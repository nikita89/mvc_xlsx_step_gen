<?php
/**
 * Web Application Event class
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace Components;
use Core\Traits;

class Events
{
	use Traits\Singleton;
	
	private
		$_subscribers = [],
		$_listeners = [],
		$_triggers = [];

    /**
     * Register instance
     */
    public static function _register()
    {
		\App\Events::getInstance();
	}
	
	/**
	 * Fire event
	 * @param Event $event
	 */
    public function fire(\App\Events\Event $event)
    {
		$event_class = get_class($event);
		if ($listeners = $this->getListeners($event_class)) {
			foreach ($listeners as $listener_class) {
				$listener = new $listener_class();
				$listener->handle($event);
			}
		}
		if ($subscribers = $this->getSubscribers($event_class)) {
			foreach ($subscribers as $handler) {
				if (is_callable($handler)) {
					$handler($event);
					continue;
				}
				$ca = explode('@', $handler);
				$subscriber_class = $ca[0];
				$handler = $ca[1];
				$subscriber = new $subscriber_class();
				$subscriber->$handler($event);
			}
		}
	}
	
	/**
	 * Set event handler
	 * @param string $action
	 * @param callable $handler
	 */
	public function on($action, $handler)
	{
		if (!array_key_exists($action, $this->_triggers)) {
			$this->_triggers[$action] = [];
		}
		if (!in_array($handler, $this->_triggers[$action])) {
			$this->_triggers[$action][]= $handler;
		}
	}
	
	/**
	 * Trigger event
	 * @param string $action
	 * @param array $event_args
	 * @return mixed
	 */
	public function trigger($action, $event_args = [])
	{
		$result = true;
		foreach($this->_triggers as $event_namespace=>$handlers) {
			if (preg_match('/^'.$event_namespace.'$/si', $action)) {
				$trace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 2);
				$result = $this->_runTriggerHandlers($action, $handlers, $event_args, $trace[1]);
			}
		}
		return $result;
	}
	
	/**
	 * Run trigger event
	 * @param string $action
	 * @param array $handlers
	 * @param array $event_args
	 * @param array $caller
	 * @return mixed
	 */
	private function _runTriggerHandlers($action, $handlers, $event_args = [], $caller)
	{
		$result = true;
		foreach ($handlers as $handler) {
			if (is_string($handler) && strpos($handler, '@')) {
				$handler = explode('@', $handler);
			}
			$result = call_user_func_array(
				$handler, ['event' => new \Core\Models\Event($action, $event_args, (object)$caller)]
			);
			if ($result === false) {
				break;
			}
		}
		return $result;
	}
	
    /**
     * Register event subscribers
	 * @param mixed $subscribers
     */
    public function registerSubscribers($subscribers)
    {
		if (!is_array($subscribers)) {
			$subscribers = [$subscribers];
		}
		foreach ($subscribers as $subscriber) {
			$subscriber::subscribe($this);
		}
	}
	
    /**
     * Register event subscribers
	 * @param string $namespace
	 * @param mixed $handler
     */
    public function subscribe($namespace, $handler)
    {
		if (!array_key_exists($namespace, $this->_subscribers)) {
			$this->_subscribers[$namespace] = [];
		}
		if (!in_array($handler, $this->_subscribers[$namespace])) {
			$this->_subscribers[$namespace][]= $handler;
		}
	}
	
    /**
     * Get event subscribers
	 * @param string $namespace
	 * @return mixed
     */
    public function getSubscribers($namespace)
    {
		if (!array_key_exists($namespace, $this->_subscribers)) {
			return null;
		}
		return $this->_subscribers[$namespace];
	}
	
    /**
     * Register event listeners
	 * @param string $event
	 * @param mixed $lsteners
     */
    public function registerListeners($event, $lsteners)
    {
		if (!is_array($lsteners)) {
			$lsteners = [$lsteners];
		}
		if (!array_key_exists($event, $this->_listeners)) {
			$this->_listeners[$event] = [];
		}
		foreach ($lsteners as $lstener) {
			if (!in_array($lstener, $this->_listeners[$event])) {
				$this->_listeners[$event][]= $lstener;
			}
		}
	}
	
    /**
     * Get event listeners
	 * @param string $namespace
	 * @return mixed
     */
    public function getListeners($namespace)
    {
		if (!array_key_exists($namespace, $this->_listeners)) {
			return null;
		}
		return $this->_listeners[$namespace];
	}
}
