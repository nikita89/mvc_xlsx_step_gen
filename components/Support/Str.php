<?php
/**
 * String operations
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace Components\Support;

abstract class Str
{
	/**
	 * Reverse string
	 * @param string $str
	 * @return string
	 */
    public static function reverse($str)
    {
		preg_match_all('/./us', self::toString($str), $ar);
		return join('', array_reverse($ar[0]));
	}
	
	/**
	 * Generate random string
	 * @param integer $length string length
	 * @return string
	 */
    public static function gen($length = 5)
    {
		return substr(
			str_shuffle(str_repeat("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz", $length)), 0, $length
		);
	}
	
	/**
	 * To Integer convert operation
	 * @param mixed $value 
	 * @return integer
	 */
    public static function toInt($value)
    {
		return (int)$value;
	}
	
	/**
	 * To Numeric convert operation
	 * @param mixed $value 
	 * @return string
	 */
    public static function toNumeric($value)
    {
		return preg_replace('#[^0-9\.]+#Uis', '', self::toString($value));
	}
	
	/**
	 * To Float convert operation
	 * @param mixed $value 
	 * @return float
	 */
    public static function toFloat($value)
    {
		return (float)$value;
	}
	
	/**
	 * To String convert operation
	 * @param mixed $value 
	 * @return string
	 */
    public static function toString($value)
    {
		if (is_array($value) || is_object($value)) {
			$value = print_r($value, 1);
		}
		return (string)$value;
	}
	
	/**
	 * To type operation
	 * @param mixed $value 
	 * @param string $type target type
	 * @return integer|float|string
	 */
    public static function to($value, $type = 'string')
    {
		switch ($type) {
			case 'int':
				return self::toInt($value);
			break;
			case 'numeric':
				return self::toNumeric(
					self::toString($value)
				);
			break;
			case 'float':
				return self::toFloat($value);
			break;
			case 'string':
				return self::toString($value);
			break;
			default:
				return self::toString($value);
		}
	}
	
	/**
	 * To md5
	 * @param mixed $value 
	 * @return string
	 */
    public static function md5($value)
    {
		return md5(self::toString($value));
	}
}
