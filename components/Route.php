<?php
/**
 * Route class
 */
namespace Components;
use Components\Support\Str,
	Core\Traits;

class Route
{
	use Traits\Singleton;
	const 
		METHODS = [
			'GET', 'HEAD', 'POST', 'PUT', 'DELETE', 'OPTIONS', 'CLI'
		],
		REGULARS = [
			'int' => '[0-9]+',
			'float' => '[0-9\.]+',
			'string' => '[a-zA-Z0-9\.\-]+'
		];
	private 
		$request,
		$method,
		$_uri,
		$_current,
		$_rules = [],
		$_patterns = [],
		$_variables = [],
		$_named_patterns = [];
	
    /**
     * Constructor
     */
    private function __construct()
    {
		$this->request = request();
		$this->method = $this->request->hasCli() ? 'CLI' : $this->request->method();
		
		foreach (static::METHODS as $method) {
			$this->_rules[$method] = [];
			$this->_patterns[$method] = [];
			$this->_variables[$method] = [];
		}		
	}
	
    /**
     * Register instance
     */
    public static function _register()
    {
		\App\Routes::getInstance();
	}
	
    /**
     * Navigate app
	 * @return void
     */
    public function navigate()
    {
		if (!is_null($this->_current)) {
			error(404, 'Router is already navigated');
		}
		if (!$rule = $this->initialize()) {
			error(404, '['.$this->method.'] '.$this->request->uri());
		}
		$_GET = array_merge($_GET, $rule->arguments);
		$_REQUEST = array_merge($_REQUEST, $rule->arguments);

		if (is_null($rule->controller)) {
			if (!is_callable($rule->action)) {
				error(500, 'Uncallable action: '.$rule->action);
			}
			return $this->runCallback(
				$rule->action, getFunctionArgs($rule->action), $rule->arguments
			);
		}
		if (preg_match_all('#{([^}]+)}#Uis', $rule->controller, $arg_matches)) {
			foreach($arg_matches[1] as $arg_match_code) {
				$rule->controller = str_replace('{'.$arg_match_code.'}', $this->request->input($arg_match_code, ''), $rule->controller);
			}
		}
		if (preg_match_all('#{([^}]+)}#Uis', $rule->action, $arg_matches)) {
			foreach($arg_matches[1] as $arg_match_code) {
				$rule->action = str_replace('{'.$arg_match_code.'}', $this->request->input($arg_match_code, ''), $rule->action);
			}
		}
		if (strpos($rule->controller, '/') !== false) {
			$ctrl_parts = explode('/', $rule->controller);
			$rule->controller = str_replace('/', '\\', $rule->controller);
		}
        $class = $rule->controller;
        $controller = new $class();
		
        if (!method_exists($controller, $rule->action)) {
			error(500, 'Method "' . $rule->action . '" not found in controller class: ' . $rule->controller);
        }
		return $this->runCallback(
			[$controller, $rule->action], getMethodArgs($controller, $rule->action), $rule->arguments
		);
	}
	
    /**
     * Execute callback
	 * @param callable $callback
	 * @param array $ask_args
	 * @param array $route_args
	 * @return void
     */
    private function runCallback($callback, $ask_args, $route_args)
    {
		$args = [];
		foreach ($ask_args as $ask_arg) {
			$value = null;
			if (array_key_exists($ask_arg->name, $route_args)) {
				$value = $route_args[$ask_arg->name];
			}
			if (!empty($ask_arg->class)) {
				$ask_class = $ask_arg->class;
				if (!$object = app()->getRegistered($ask_class, $value)) {
					$not_found_method = 'notFound';
					if (method_exists($ask_class, $not_found_method)) {
						return $ask_class::$not_found_method();
					}
					error(404, $ask_arg->name.' not found');
				}
				$args[$ask_arg->name] = $object;
				$_REQUEST[$ask_arg->name] = $object;
			} else {
				$args[$ask_arg->name] = $value;
			}
		}
		return call_user_func_array(
			$callback, $args
		);
	}
			
    /**
     * Init Router
	 * @return void
     */
    private function initialize()
    {
		$this->_uri = substr(
			$this->request->uri(), strlen(config('appuri'))+1
		);
		if (strpos($this->_uri, 'index.php') === 0) {
			$this->_uri = substr($this->_uri, 9);
		}
		if (strpos($this->_uri, '?') !== false) {
			$uriexp = explode('?', $this->_uri);
			$this->_uri = $uriexp[0];
		}
		$this->_uri = rtrim($this->_uri, '/');
		if (empty($this->_uri)) {
			$this->_uri = '/';
		}
		return $this->getRule();
	}
	
    /**
     * Get current rule
	 * @return object|null
     */
    private function getRule()
    {
		foreach ($this->_patterns[$this->method] as $uri=>$pattern) {
			
			if (preg_match('#'.$pattern.'#Uis', $this->_uri, $vars)) {
				
				$this->_current = (object)array(
					'controller' => null,
					'action' => null,
					'arguments' => []
				);
				if (is_callable($this->_rules[$this->method][$uri])) {
					$this->_current->action = $this->_rules[$this->method][$uri];
				} else {
					$ca = explode('@', $this->_rules[$this->method][$uri]);
					$this->_current->controller = $ca[0];
					$this->_current->action = $ca[1];
				}
				if (!empty($this->_variables[$this->method][$uri])) {
					array_shift($vars);
					foreach ($this->_variables[$this->method][$uri] as $k=>$var) {
						$type = 'string';
						if (isset(static::REGULARS[$var->type])) {
							$type = $var->type;
						}
						$this->_current->arguments[$var->name] = Str::to($vars[$k], $type);
					}
				}
				return $this->getCurrent();
			}
		}
		return null;
	}
	
    /**
     * Get current rule
	 * @return object|null
     */
    public function getCurrent()
    {
		return $this->_current;
	}
	
    /**
     * Routing to named rule
	 * @param string $name Rule name
	 * @param array $args Rule args
     */
    public function to($name, $args = [])
    {
		Response::location(
			$this->url($name, $args)
		);
	}
	
    /**
     * Get route url
	 * @param string $name Rule name
	 * @param array $args Rule args
     */
    public function url($name, $args = [])
    {
		if (!isset($this->_named_patterns[$name])) {
			throw new \Exception('Unregistered route: '.$name);
		}
		$url_args = [];
		$url = $this->_named_patterns[$name];

		foreach ($args as $key=>$val) {
			if (preg_match('#(\{'.$key.'=[^\}]+\})#Uis', $url)) {
				$url = preg_replace('#(\{'.$key.'=[^\}]+\})#Uis', $val, $url);
			} else {
				$url_args[$key]= $val;
			}
		}
		if ($url == '/') {
			$url = '';
		}
		if (!empty($url_args)) {
			$url .= '?'.http_build_query($url_args);
		}
		return APP_URL . '/' . $url;
	}

   /**
     * Add custom methods route rule
	 * @param array $methods request methods
	 * @param string $url request url
	 * @param string $callback rule callback
	 * @param null|string $name rule name
     */
    public function methods($methods, $url, $callback, $name = null)
    {
		foreach ($methods as $method) {
			$this->$method($url, $callback, $name);
		}
	}
	
    /**
     * Add Console route rule
	 * @param string $url request url
	 * @param string $callback rule callback
	 * @param null|string $name rule name
     */
    public function cli($url, $callback, $name = null)
    {
		$this->_insertRule('CLI', $url, $callback);
		if (!is_null($name)) {
			$this->_named_patterns[$name] = $url;
		}
	}
	
    /**
     * Add GET route rule
	 * @param string $url request url
	 * @param string $callback rule callback
	 * @param null|string $name rule name
     */
    public function get($url, $callback, $name = null)
    {
		$this->_insertRule('GET', $url, $callback);
		if (!is_null($name)) {
			$this->_named_patterns[$name] = $url;
		}
	}
	
    /**
     * Add POST route rule
	 * @param string $url request url
	 * @param string $callback rule callback
     */
    public function post($url, $callback, $name = null)
    {
		$this->_insertRule('POST', $url, $callback);
		if (!is_null($name)) {
			$this->_named_patterns[$name] = $url;
		}
	}
	
	
    /**
     * Add HEAD route rule
	 * @param string $url request url
	 * @param string $callback rule callback
     */
    public static function head($url, $callback)
    {
		$this->_insertRule('HEAD', $url, $callback);
	}
	
    /**
     * Add PUT route rule
	 * @param string $url request url
	 * @param string $callback rule callback
     */
    public function put($url, $callback)
    {
		$this->_insertRule('PUT', $url, $callback);
	}
	
    /**
     * Add DELETE route rule
	 * @param string $url request url
	 * @param string $callback rule callback
     */
    public function delete($url, $callback)
    {
		$this->_insertRule('DELETE', $url, $callback);
	}
	
    /**
     * Add OPTIONS route rule
	 * @param string $url request url
	 * @param string $callback rule callback
     */
    public function options($url, $callback)
    {
		$this->_insertRule('OPTIONS', $url, $callback);
	}
	
	
    /**
     * Add route rule
	 * @param string $method request method
	 * @param string $url request url
	 * @param string $callback rule callback
     */
    private function _insertRule($method, $url, $callback)
    {
		if (isset($this->_rules[$method][$url])) {
			return false;
		}
		$this->_rules[$method][$url] = $callback;
		$this->_variables[$method][$url] = [];
		$regular_url = $url;
			
		preg_match_all('#\{(.+)\}#Uis', $url, $regulars);
		foreach ($regulars[1] as $k=>$reg) {
			
			if (!strpos($reg, '=')) {
				$reg .= '=string';
			}
			$reg = explode('=', $reg);
			$regular = $reg[1];
			if (isset(static::REGULARS[$reg[1]])) {
				$regular = static::REGULARS[$reg[1]];
			}
			$regular_url = str_replace($regulars[0][$k], '('.$regular.')', $regular_url);
			$this->_variables[$method][$url][$k] = (object)[
				'name' => $reg[0],
				'type' => $reg[1],
			];				
		}
		$this->_patterns[$method][$url] = '^'.$regular_url.'$';
	}
}