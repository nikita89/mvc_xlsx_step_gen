<?php
/**
 * Cache manager
 */
namespace Components;
use Components\Support\Str;

class Cache
{
	private $FM,
			$category;
	
    public function __construct($category = 'Default')
    {
		if (!is_string($category) || empty($category)) {
			throw new \Exception('Сache category must be a non-empty string');
		}
		$this->category = $category;
		$this->FM = fileManager('/app/Storage/Cache/'.$this->category.'/');
		if (!$this->FM->exists()) {
			$this->FM->createDir();
		}
	}
	
    /**
     * Get current cache category
	 * @return bool
     */
    public function getCategory()
    {
		return $this->category;
	}
	
    /**
     * Has cache isset
	 * @param string $key 
	 * @return bool
     */
    public function has($key)
    {
		$cache_file = $this->_clear_key($key);
		return $this->FM->exists($cache_file);
	}
	
    /**
     * Cache modified time
	 * @param string $key 
	 * @return integer|bool
     */
    public function modified($key, $default = false)
    {
		$cache_file = $this->_clear_key($key);
		if (!$this->has($key)) {
			return $default;
		}
		return $this->FM->getModified($cache_file);
	}
	
    /**
     * Get cache data
	 * @param string $key 
	 * @param mixed $default 
	 * @return mixed
     */
    public function get($key, $default = null)
    {
		$cache_file = $this->_clear_key($key);
		if (!$this->has($key)) {
			return $default;
		}
		return @unserialize($this->FM->get($cache_file));
	}
	
    /**
     * Set cache data
	 * @param string $key 
	 * @param mixed $data 
	 * @return bool
     */
    public function set($key, $data)
    {
		$cache_file = $this->_clear_key($key);
		return $this->FM->put($cache_file, serialize($data));
	}
	
    /**
     * Remove cache files
	 * @param mixed $key 
	 * @return bool
     */
    public function clear($key = null)
    {
		if (is_null($key)) {
			$cache_files = $this->FM->search('*.cache');
			foreach($cache_files as $file) {
				$this->FM->remove($file);
			}
			return true;
		}
		$cache_file = $this->_clear_key($key);
		return $this->FM->remove($cache_file);
	}
	
    /**
     * Clear cache filename
	 * @param string $key 
	 * @return string
     */
    private function _clear_key($key)
    {
		return Str::md5($key).'.cache';
	}
}