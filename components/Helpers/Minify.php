<?php
/**
 * Minify HTML, JS
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace Services;
 
abstract class Minify {
	/**
	 * Минификация HTML
	 */
	public static function html( $html = '' ){
		
		$html = preg_replace( '#[\t\r\n]+#Uis', '', $html );
		$html = preg_replace( '#>([\s]+)<#Uis', '><', $html );
		return trim( $html );
	}
	
	/**
	 * Минификация JS
	 */
	public static function js( $js = '' ){
		
		$query = new Http\Query('https://closure-compiler.appspot.com/compile');
		$resp = $query->post(array(
			'output_format' => 'json',
			'output_info' => 'compiled_code',
			'compilation_level' => 'SIMPLE_OPTIMIZATIONS',
			'warning_level' => 'default',
			'output_file_name' => 'compiled.js',
			'js_code' => $js
		));
		if( !empty( $resp->data )){
			if( $data = json_decode( $resp->data )){
				return $data->compiledCode;
			}
		}
		return trim( $js );
	}

	/**
	 * Удаление пробелов
	 */
	public static function stripSpaces( $content = '' ){
		
		return preg_replace( '#[\s]+#Uis', '', $content );
	}
}