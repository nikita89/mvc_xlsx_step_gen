<?php
/**
 * Web Application Email class
 * @author Vlad Ionov <vlad@f5.com.ru>
 */
namespace Components;
use PHPMailer\PHPMailer\PHPMailer;

class Email
{
	public 
		$tpl,
		$subject = '',
		$body = '';
	protected 
		$mailer,
		$error;
		
    /**
     * Constructor
     */
    public function __construct()
    {
		$this->mailer = new PHPMailer;
		$this->mailer->CharSet = 'utf-8';
		$this->tpl = new Email\Template($this);
		$this->tpl->load('default');
		
		$from = config('email.from_default');
		$this->from($from->email, $from->name);
		$this->mailer->isHTML(true);
		$this->mailer->isSMTP();
		$this->mailer->SMTPOptions = array(
			'ssl' => array(
				'verify_peer' => false,
				'verify_peer_name' => false,
				'allow_self_signed' => true
			)
		);
		$this->mailer->Host = $from->smtp->host;
		$this->mailer->Port = $from->smtp->port;
		$this->mailer->SMTPAuth = $from->smtp->auth;
		$this->mailer->SMTPSecure = $from->smtp->secure;
		$this->mailer->Username = $from->smtp->login;
		$this->mailer->Password = $from->smtp->pass;
    }
	
    /**
     * Set from address
	 * @param string $email from address
	 * @param string $name from name
	 * @return Email
     */
	public function from($email, $name = null)
	{
		if (!empty($email)) {
			if (is_null($name)) {
				$emailexp = explode('@', $email);
				$name = $emailexp[0];
			}
			$this->mailer->setFrom($email, $name);
			$this->tpl->set('from_email', $email);
			$this->tpl->set('from_name', $name);
		}
		return $this;
	}
	
    /**
     * Set to address
	 * @param string $email to address
	 * @param string $name to name
	 * @return Email
     */
	public function to($email, $name = null)
	{
		if (!empty($email)) {
			if (is_null($name)) {
				$emailexp = explode('@', $email);
				$name = $emailexp[0];
			}
			$this->mailer->addAddress($email, $name);
		}
		return $this;
	}
	
    /**
     * Set subject
	 * @param string $text email subject
	 * @return Email
     */
	public function subject($text)
	{
		$this->subject = (string)$text;
		$this->tpl->set('subject', $this->subject);
		return $this;
	}
	
    /**
     * Set body
	 * @param string $html email content
	 * @return Email
     */
	public function body($html)
	{
		$this->body = (string)$html;
		return $this;
	}
	
    /**
     * Set copy address
	 * @param string|array $emails email address
	 * @return Email
     */
	public function cc($emails)
	{
		if (!empty($emails)) {
			if (!is_array($emails)) {
				$emails = [$emails];
			}
			foreach ($emails as $email) {
				$this->mailer->addCC($email);
			}
		}
		return $this;
	}
	
    /**
     * Set hidden copy address
	 * @param string|array $emails email address
	 * @return Email
     */
	public function bcc($emails)
	{
		if (!empty($emails)) {
			if (!is_array($emails)) {
				$emails = [$emails];
			}
			foreach ($emails as $email) {
				$this->mailer->addBCC($email);
			}
		}
		return $this;
	}
	
    /**
     * Attach file
	 * @param string $path
	 * @param mixed $name
	 * @return Email
     */
	public function attach($path, $name = null)
	{
		if (is_null($name)) {
			$nameexp = explode('/', $path);
			$name = end($nameexp);
		}
		$this->mailer->addAttachment($path, $name);
		return $this;
	}
	
    /**
     * Send email
	 * @param integer $debug smtp debug
	 * @return bool
     */
	public function send($debug = 0)
	{
		$this->tpl->build();
		$this->mailer->SMTPDebug = $debug;
		$this->mailer->Subject = $this->subject;
		$this->mailer->Body = $this->body;

		if ($this->mailer->send()) {
			return true;
		}
		$this->error = $this->mailer->ErrorInfo;
		return false;
	}
	
    /**
     * Get error text
	 * @return string
     */
	public function getError()
	{
		return (string)$this->error;
	}
}
